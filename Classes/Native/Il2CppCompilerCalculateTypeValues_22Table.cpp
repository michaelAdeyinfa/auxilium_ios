﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EnvMapAnimator
struct EnvMapAnimator_t1140999784;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<TMPro.KerningPair>
struct List_1_t3742930331;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Func`2<TMPro.KerningPair,System.UInt32>
struct Func_2_t2163425431;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t4243939292;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Predicate`1<System.String>
struct Predicate_1_t2672744813;
// System.Predicate`1<UnityEngine.GameObject>
struct Predicate_1_t1938930743;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// TMPro.Examples.Benchmark01
struct Benchmark01_t1571072624;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3264177817;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t3617420994;
// TMPro.Examples.SkewTextExample
struct SkewTextExample_t3460249701;
// TMPro.Examples.TeleType
struct TeleType_t2409835159;
// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t3766250034;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t845872552;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t648826345;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t3678055768;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2496920137;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t3552942253;
// TMPro.TextContainer
struct TextContainer_t97923372;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// assets.floorLogic.floorLogic
struct floorLogic_t3814374729;
// assets.pathLogic.pathLogic
struct pathLogic_t3937024697;
// assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0
struct U3CCalculatePathU3Ec__Iterator0_t2361313786;
// assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey1
struct U3CCalculatePathU3Ec__AnonStorey1_t2938898595;




#ifndef U3CMODULEU3E_T692745544_H
#define U3CMODULEU3E_T692745544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745544 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745544_H
#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2216151886_H
#define U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2216151886  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t1571072624 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24this_1)); }
	inline Benchmark01_t1571072624 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t1571072624 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t1571072624 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2622988697_H
#define U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2622988697  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3264177817 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24this_1)); }
	inline Benchmark01_UGUI_t3264177817 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3264177817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3264177817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifndef U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#define U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0
struct  U3CAnimatePropertiesU3Ec__Iterator0_t4041402054  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::<glowPower>__1
	float ___U3CglowPowerU3E__1_0;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$this
	ShaderPropAnimator_t3617420994 * ___U24this_1;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CglowPowerU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U3CglowPowerU3E__1_0)); }
	inline float get_U3CglowPowerU3E__1_0() const { return ___U3CglowPowerU3E__1_0; }
	inline float* get_address_of_U3CglowPowerU3E__1_0() { return &___U3CglowPowerU3E__1_0; }
	inline void set_U3CglowPowerU3E__1_0(float value)
	{
		___U3CglowPowerU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24this_1)); }
	inline ShaderPropAnimator_t3617420994 * get_U24this_1() const { return ___U24this_1; }
	inline ShaderPropAnimator_t3617420994 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShaderPropAnimator_t3617420994 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3341539328_H
#define U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3341539328  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_0;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<counter>__0
	int32_t ___U3CcounterU3E__0_1;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_2;
	// TMPro.Examples.TeleType TMPro.Examples.TeleType/<Start>c__Iterator0::$this
	TeleType_t2409835159 * ___U24this_3;
	// System.Object TMPro.Examples.TeleType/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TMPro.Examples.TeleType/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CtotalVisibleCharactersU3E__0_0)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_0() const { return ___U3CtotalVisibleCharactersU3E__0_0; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_0() { return &___U3CtotalVisibleCharactersU3E__0_0; }
	inline void set_U3CtotalVisibleCharactersU3E__0_0(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CcounterU3E__0_1)); }
	inline int32_t get_U3CcounterU3E__0_1() const { return ___U3CcounterU3E__0_1; }
	inline int32_t* get_address_of_U3CcounterU3E__0_1() { return &___U3CcounterU3E__0_1; }
	inline void set_U3CcounterU3E__0_1(int32_t value)
	{
		___U3CcounterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CvisibleCountU3E__0_2)); }
	inline int32_t get_U3CvisibleCountU3E__0_2() const { return ___U3CvisibleCountU3E__0_2; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_2() { return &___U3CvisibleCountU3E__0_2; }
	inline void set_U3CvisibleCountU3E__0_2(int32_t value)
	{
		___U3CvisibleCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24this_3)); }
	inline TeleType_t2409835159 * get_U24this_3() const { return ___U24this_3; }
	inline TeleType_t2409835159 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TeleType_t2409835159 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifndef U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#define U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0
struct  U3CRevealCharactersU3Ec__Iterator0_t860191687  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// TMPro.TMP_TextInfo TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_3;
	// TMPro.Examples.TextConsoleSimulator TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$this
	TextConsoleSimulator_t3766250034 * ___U24this_4;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtextInfoU3E__0_1)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_1() const { return ___U3CtextInfoU3E__0_1; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_1() { return &___U3CtextInfoU3E__0_1; }
	inline void set_U3CtextInfoU3E__0_1(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CvisibleCountU3E__0_3)); }
	inline int32_t get_U3CvisibleCountU3E__0_3() const { return ___U3CvisibleCountU3E__0_3; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_3() { return &___U3CvisibleCountU3E__0_3; }
	inline void set_U3CvisibleCountU3E__0_3(int32_t value)
	{
		___U3CvisibleCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24this_4)); }
	inline TextConsoleSimulator_t3766250034 * get_U24this_4() const { return ___U24this_4; }
	inline TextConsoleSimulator_t3766250034 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TextConsoleSimulator_t3766250034 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifndef U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#define U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1
struct  U3CRevealWordsU3Ec__Iterator1_t1343183262  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalWordCount>__0
	int32_t ___U3CtotalWordCountU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<counter>__0
	int32_t ___U3CcounterU3E__0_3;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<currentWord>__0
	int32_t ___U3CcurrentWordU3E__0_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_5;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtotalWordCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalWordCountU3E__0_1)); }
	inline int32_t get_U3CtotalWordCountU3E__0_1() const { return ___U3CtotalWordCountU3E__0_1; }
	inline int32_t* get_address_of_U3CtotalWordCountU3E__0_1() { return &___U3CtotalWordCountU3E__0_1; }
	inline void set_U3CtotalWordCountU3E__0_1(int32_t value)
	{
		___U3CtotalWordCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcounterU3E__0_3)); }
	inline int32_t get_U3CcounterU3E__0_3() const { return ___U3CcounterU3E__0_3; }
	inline int32_t* get_address_of_U3CcounterU3E__0_3() { return &___U3CcounterU3E__0_3; }
	inline void set_U3CcounterU3E__0_3(int32_t value)
	{
		___U3CcounterU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentWordU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcurrentWordU3E__0_4)); }
	inline int32_t get_U3CcurrentWordU3E__0_4() const { return ___U3CcurrentWordU3E__0_4; }
	inline int32_t* get_address_of_U3CcurrentWordU3E__0_4() { return &___U3CcurrentWordU3E__0_4; }
	inline void set_U3CcurrentWordU3E__0_4(int32_t value)
	{
		___U3CcurrentWordU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CvisibleCountU3E__0_5)); }
	inline int32_t get_U3CvisibleCountU3E__0_5() const { return ___U3CvisibleCountU3E__0_5; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_5() { return &___U3CvisibleCountU3E__0_5; }
	inline void set_U3CvisibleCountU3E__0_5(int32_t value)
	{
		___U3CvisibleCountU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifndef KERNINGTABLE_T2322366871_H
#define KERNINGTABLE_T2322366871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable
struct  KerningTable_t2322366871  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.KerningPair> TMPro.KerningTable::kerningPairs
	List_1_t3742930331 * ___kerningPairs_0;

public:
	inline static int32_t get_offset_of_kerningPairs_0() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871, ___kerningPairs_0)); }
	inline List_1_t3742930331 * get_kerningPairs_0() const { return ___kerningPairs_0; }
	inline List_1_t3742930331 ** get_address_of_kerningPairs_0() { return &___kerningPairs_0; }
	inline void set_kerningPairs_0(List_1_t3742930331 * value)
	{
		___kerningPairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___kerningPairs_0), value);
	}
};

struct KerningTable_t2322366871_StaticFields
{
public:
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache0
	Func_2_t2163425431 * ___U3CU3Ef__amU24cache0_1;
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache1
	Func_2_t2163425431 * ___U3CU3Ef__amU24cache1_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t2163425431 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t2163425431 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t2163425431 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Func_2_t2163425431 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Func_2_t2163425431 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Func_2_t2163425431 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGTABLE_T2322366871_H
#ifndef U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#define U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1
struct  U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#ifndef U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#define U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddKerningPair>c__AnonStorey0
struct  U3CAddKerningPairU3Ec__AnonStorey0_t2688361982  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t2688361982, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t2688361982, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#ifndef U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#define U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2
struct  U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#ifndef SHADERUTILITIES_T714255158_H
#define SHADERUTILITIES_T714255158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t714255158  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t714255158_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_Padding
	int32_t ___ID_Padding_16;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_18;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_19;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_20;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_21;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_22;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_23;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_26;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_27;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_28;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_30;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_31;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_32;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_33;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_34;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_35;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_36;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_37;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_38;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_42;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_43;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_46;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_47;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_48;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_49;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_50;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_51;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_53;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_54;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_55;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_56;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_57;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_58;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_59;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_Padding_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_Padding_16)); }
	inline int32_t get_ID_Padding_16() const { return ___ID_Padding_16; }
	inline int32_t* get_address_of_ID_Padding_16() { return &___ID_Padding_16; }
	inline void set_ID_Padding_16(int32_t value)
	{
		___ID_Padding_16 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GradientScale_17)); }
	inline int32_t get_ID_GradientScale_17() const { return ___ID_GradientScale_17; }
	inline int32_t* get_address_of_ID_GradientScale_17() { return &___ID_GradientScale_17; }
	inline void set_ID_GradientScale_17(int32_t value)
	{
		___ID_GradientScale_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleX_18)); }
	inline int32_t get_ID_ScaleX_18() const { return ___ID_ScaleX_18; }
	inline int32_t* get_address_of_ID_ScaleX_18() { return &___ID_ScaleX_18; }
	inline void set_ID_ScaleX_18(int32_t value)
	{
		___ID_ScaleX_18 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleY_19)); }
	inline int32_t get_ID_ScaleY_19() const { return ___ID_ScaleY_19; }
	inline int32_t* get_address_of_ID_ScaleY_19() { return &___ID_ScaleY_19; }
	inline void set_ID_ScaleY_19(int32_t value)
	{
		___ID_ScaleY_19 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_PerspectiveFilter_20)); }
	inline int32_t get_ID_PerspectiveFilter_20() const { return ___ID_PerspectiveFilter_20; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_20() { return &___ID_PerspectiveFilter_20; }
	inline void set_ID_PerspectiveFilter_20(int32_t value)
	{
		___ID_PerspectiveFilter_20 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureWidth_21)); }
	inline int32_t get_ID_TextureWidth_21() const { return ___ID_TextureWidth_21; }
	inline int32_t* get_address_of_ID_TextureWidth_21() { return &___ID_TextureWidth_21; }
	inline void set_ID_TextureWidth_21(int32_t value)
	{
		___ID_TextureWidth_21 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureHeight_22)); }
	inline int32_t get_ID_TextureHeight_22() const { return ___ID_TextureHeight_22; }
	inline int32_t* get_address_of_ID_TextureHeight_22() { return &___ID_TextureHeight_22; }
	inline void set_ID_TextureHeight_22(int32_t value)
	{
		___ID_TextureHeight_22 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_BevelAmount_23)); }
	inline int32_t get_ID_BevelAmount_23() const { return ___ID_BevelAmount_23; }
	inline int32_t* get_address_of_ID_BevelAmount_23() { return &___ID_BevelAmount_23; }
	inline void set_ID_BevelAmount_23(int32_t value)
	{
		___ID_BevelAmount_23 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowColor_24)); }
	inline int32_t get_ID_GlowColor_24() const { return ___ID_GlowColor_24; }
	inline int32_t* get_address_of_ID_GlowColor_24() { return &___ID_GlowColor_24; }
	inline void set_ID_GlowColor_24(int32_t value)
	{
		___ID_GlowColor_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOffset_25)); }
	inline int32_t get_ID_GlowOffset_25() const { return ___ID_GlowOffset_25; }
	inline int32_t* get_address_of_ID_GlowOffset_25() { return &___ID_GlowOffset_25; }
	inline void set_ID_GlowOffset_25(int32_t value)
	{
		___ID_GlowOffset_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowPower_26)); }
	inline int32_t get_ID_GlowPower_26() const { return ___ID_GlowPower_26; }
	inline int32_t* get_address_of_ID_GlowPower_26() { return &___ID_GlowPower_26; }
	inline void set_ID_GlowPower_26(int32_t value)
	{
		___ID_GlowPower_26 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOuter_27)); }
	inline int32_t get_ID_GlowOuter_27() const { return ___ID_GlowOuter_27; }
	inline int32_t* get_address_of_ID_GlowOuter_27() { return &___ID_GlowOuter_27; }
	inline void set_ID_GlowOuter_27(int32_t value)
	{
		___ID_GlowOuter_27 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_LightAngle_28)); }
	inline int32_t get_ID_LightAngle_28() const { return ___ID_LightAngle_28; }
	inline int32_t* get_address_of_ID_LightAngle_28() { return &___ID_LightAngle_28; }
	inline void set_ID_LightAngle_28(int32_t value)
	{
		___ID_LightAngle_28 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMap_29)); }
	inline int32_t get_ID_EnvMap_29() const { return ___ID_EnvMap_29; }
	inline int32_t* get_address_of_ID_EnvMap_29() { return &___ID_EnvMap_29; }
	inline void set_ID_EnvMap_29(int32_t value)
	{
		___ID_EnvMap_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrix_30)); }
	inline int32_t get_ID_EnvMatrix_30() const { return ___ID_EnvMatrix_30; }
	inline int32_t* get_address_of_ID_EnvMatrix_30() { return &___ID_EnvMatrix_30; }
	inline void set_ID_EnvMatrix_30(int32_t value)
	{
		___ID_EnvMatrix_30 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrixRotation_31)); }
	inline int32_t get_ID_EnvMatrixRotation_31() const { return ___ID_EnvMatrixRotation_31; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_31() { return &___ID_EnvMatrixRotation_31; }
	inline void set_ID_EnvMatrixRotation_31(int32_t value)
	{
		___ID_EnvMatrixRotation_31 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskCoord_32)); }
	inline int32_t get_ID_MaskCoord_32() const { return ___ID_MaskCoord_32; }
	inline int32_t* get_address_of_ID_MaskCoord_32() { return &___ID_MaskCoord_32; }
	inline void set_ID_MaskCoord_32(int32_t value)
	{
		___ID_MaskCoord_32 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ClipRect_33)); }
	inline int32_t get_ID_ClipRect_33() const { return ___ID_ClipRect_33; }
	inline int32_t* get_address_of_ID_ClipRect_33() { return &___ID_ClipRect_33; }
	inline void set_ID_ClipRect_33(int32_t value)
	{
		___ID_ClipRect_33 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessX_34)); }
	inline int32_t get_ID_MaskSoftnessX_34() const { return ___ID_MaskSoftnessX_34; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_34() { return &___ID_MaskSoftnessX_34; }
	inline void set_ID_MaskSoftnessX_34(int32_t value)
	{
		___ID_MaskSoftnessX_34 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessY_35)); }
	inline int32_t get_ID_MaskSoftnessY_35() const { return ___ID_MaskSoftnessY_35; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_35() { return &___ID_MaskSoftnessY_35; }
	inline void set_ID_MaskSoftnessY_35(int32_t value)
	{
		___ID_MaskSoftnessY_35 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetX_36)); }
	inline int32_t get_ID_VertexOffsetX_36() const { return ___ID_VertexOffsetX_36; }
	inline int32_t* get_address_of_ID_VertexOffsetX_36() { return &___ID_VertexOffsetX_36; }
	inline void set_ID_VertexOffsetX_36(int32_t value)
	{
		___ID_VertexOffsetX_36 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetY_37)); }
	inline int32_t get_ID_VertexOffsetY_37() const { return ___ID_VertexOffsetY_37; }
	inline int32_t* get_address_of_ID_VertexOffsetY_37() { return &___ID_VertexOffsetY_37; }
	inline void set_ID_VertexOffsetY_37(int32_t value)
	{
		___ID_VertexOffsetY_37 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UseClipRect_38)); }
	inline int32_t get_ID_UseClipRect_38() const { return ___ID_UseClipRect_38; }
	inline int32_t* get_address_of_ID_UseClipRect_38() { return &___ID_UseClipRect_38; }
	inline void set_ID_UseClipRect_38(int32_t value)
	{
		___ID_UseClipRect_38 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilID_39)); }
	inline int32_t get_ID_StencilID_39() const { return ___ID_StencilID_39; }
	inline int32_t* get_address_of_ID_StencilID_39() { return &___ID_StencilID_39; }
	inline void set_ID_StencilID_39(int32_t value)
	{
		___ID_StencilID_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilOp_40)); }
	inline int32_t get_ID_StencilOp_40() const { return ___ID_StencilOp_40; }
	inline int32_t* get_address_of_ID_StencilOp_40() { return &___ID_StencilOp_40; }
	inline void set_ID_StencilOp_40(int32_t value)
	{
		___ID_StencilOp_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilComp_41)); }
	inline int32_t get_ID_StencilComp_41() const { return ___ID_StencilComp_41; }
	inline int32_t* get_address_of_ID_StencilComp_41() { return &___ID_StencilComp_41; }
	inline void set_ID_StencilComp_41(int32_t value)
	{
		___ID_StencilComp_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilReadMask_42)); }
	inline int32_t get_ID_StencilReadMask_42() const { return ___ID_StencilReadMask_42; }
	inline int32_t* get_address_of_ID_StencilReadMask_42() { return &___ID_StencilReadMask_42; }
	inline void set_ID_StencilReadMask_42(int32_t value)
	{
		___ID_StencilReadMask_42 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilWriteMask_43)); }
	inline int32_t get_ID_StencilWriteMask_43() const { return ___ID_StencilWriteMask_43; }
	inline int32_t* get_address_of_ID_StencilWriteMask_43() { return &___ID_StencilWriteMask_43; }
	inline void set_ID_StencilWriteMask_43(int32_t value)
	{
		___ID_StencilWriteMask_43 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ShaderFlags_44)); }
	inline int32_t get_ID_ShaderFlags_44() const { return ___ID_ShaderFlags_44; }
	inline int32_t* get_address_of_ID_ShaderFlags_44() { return &___ID_ShaderFlags_44; }
	inline void set_ID_ShaderFlags_44(int32_t value)
	{
		___ID_ShaderFlags_44 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_A_45)); }
	inline int32_t get_ID_ScaleRatio_A_45() const { return ___ID_ScaleRatio_A_45; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_45() { return &___ID_ScaleRatio_A_45; }
	inline void set_ID_ScaleRatio_A_45(int32_t value)
	{
		___ID_ScaleRatio_A_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_B_46)); }
	inline int32_t get_ID_ScaleRatio_B_46() const { return ___ID_ScaleRatio_B_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_46() { return &___ID_ScaleRatio_B_46; }
	inline void set_ID_ScaleRatio_B_46(int32_t value)
	{
		___ID_ScaleRatio_B_46 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_C_47)); }
	inline int32_t get_ID_ScaleRatio_C_47() const { return ___ID_ScaleRatio_C_47; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_47() { return &___ID_ScaleRatio_C_47; }
	inline void set_ID_ScaleRatio_C_47(int32_t value)
	{
		___ID_ScaleRatio_C_47 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Bevel_48)); }
	inline String_t* get_Keyword_Bevel_48() const { return ___Keyword_Bevel_48; }
	inline String_t** get_address_of_Keyword_Bevel_48() { return &___Keyword_Bevel_48; }
	inline void set_Keyword_Bevel_48(String_t* value)
	{
		___Keyword_Bevel_48 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_48), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Glow_49)); }
	inline String_t* get_Keyword_Glow_49() const { return ___Keyword_Glow_49; }
	inline String_t** get_address_of_Keyword_Glow_49() { return &___Keyword_Glow_49; }
	inline void set_Keyword_Glow_49(String_t* value)
	{
		___Keyword_Glow_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Underlay_50)); }
	inline String_t* get_Keyword_Underlay_50() const { return ___Keyword_Underlay_50; }
	inline String_t** get_address_of_Keyword_Underlay_50() { return &___Keyword_Underlay_50; }
	inline void set_Keyword_Underlay_50(String_t* value)
	{
		___Keyword_Underlay_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_50), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Ratios_51)); }
	inline String_t* get_Keyword_Ratios_51() const { return ___Keyword_Ratios_51; }
	inline String_t** get_address_of_Keyword_Ratios_51() { return &___Keyword_Ratios_51; }
	inline void set_Keyword_Ratios_51(String_t* value)
	{
		___Keyword_Ratios_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_51), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_SOFT_52)); }
	inline String_t* get_Keyword_MASK_SOFT_52() const { return ___Keyword_MASK_SOFT_52; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_52() { return &___Keyword_MASK_SOFT_52; }
	inline void set_Keyword_MASK_SOFT_52(String_t* value)
	{
		___Keyword_MASK_SOFT_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_HARD_53)); }
	inline String_t* get_Keyword_MASK_HARD_53() const { return ___Keyword_MASK_HARD_53; }
	inline String_t** get_address_of_Keyword_MASK_HARD_53() { return &___Keyword_MASK_HARD_53; }
	inline void set_Keyword_MASK_HARD_53(String_t* value)
	{
		___Keyword_MASK_HARD_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_53), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_TEX_54)); }
	inline String_t* get_Keyword_MASK_TEX_54() const { return ___Keyword_MASK_TEX_54; }
	inline String_t** get_address_of_Keyword_MASK_TEX_54() { return &___Keyword_MASK_TEX_54; }
	inline void set_Keyword_MASK_TEX_54(String_t* value)
	{
		___Keyword_MASK_TEX_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_54), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Outline_55)); }
	inline String_t* get_Keyword_Outline_55() const { return ___Keyword_Outline_55; }
	inline String_t** get_address_of_Keyword_Outline_55() { return &___Keyword_Outline_55; }
	inline void set_Keyword_Outline_55(String_t* value)
	{
		___Keyword_Outline_55 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_55), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_ZTestMode_56)); }
	inline String_t* get_ShaderTag_ZTestMode_56() const { return ___ShaderTag_ZTestMode_56; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_56() { return &___ShaderTag_ZTestMode_56; }
	inline void set_ShaderTag_ZTestMode_56(String_t* value)
	{
		___ShaderTag_ZTestMode_56 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_56), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_CullMode_57)); }
	inline String_t* get_ShaderTag_CullMode_57() const { return ___ShaderTag_CullMode_57; }
	inline String_t** get_address_of_ShaderTag_CullMode_57() { return &___ShaderTag_CullMode_57; }
	inline void set_ShaderTag_CullMode_57(String_t* value)
	{
		___ShaderTag_CullMode_57 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_57), value);
	}

	inline static int32_t get_offset_of_m_clamp_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___m_clamp_58)); }
	inline float get_m_clamp_58() const { return ___m_clamp_58; }
	inline float* get_address_of_m_clamp_58() { return &___m_clamp_58; }
	inline void set_m_clamp_58(float value)
	{
		___m_clamp_58 = value;
	}

	inline static int32_t get_offset_of_isInitialized_59() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___isInitialized_59)); }
	inline bool get_isInitialized_59() const { return ___isInitialized_59; }
	inline bool* get_address_of_isInitialized_59() { return &___isInitialized_59; }
	inline void set_isInitialized_59(bool value)
	{
		___isInitialized_59 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T714255158_H
#ifndef TMP_FONTUTILITIES_T2599150238_H
#define TMP_FONTUTILITIES_T2599150238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontUtilities
struct  TMP_FontUtilities_t2599150238  : public RuntimeObject
{
public:

public:
};

struct TMP_FontUtilities_t2599150238_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontUtilities::k_searchedFontAssets
	List_1_t128053199 * ___k_searchedFontAssets_0;

public:
	inline static int32_t get_offset_of_k_searchedFontAssets_0() { return static_cast<int32_t>(offsetof(TMP_FontUtilities_t2599150238_StaticFields, ___k_searchedFontAssets_0)); }
	inline List_1_t128053199 * get_k_searchedFontAssets_0() const { return ___k_searchedFontAssets_0; }
	inline List_1_t128053199 ** get_address_of_k_searchedFontAssets_0() { return &___k_searchedFontAssets_0; }
	inline void set_k_searchedFontAssets_0(List_1_t128053199 * value)
	{
		___k_searchedFontAssets_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedFontAssets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTUTILITIES_T2599150238_H
#ifndef ANALYTICSEVENT_T4058973021_H
#define ANALYTICSEVENT_T4058973021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEvent
struct  AnalyticsEvent_t4058973021  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEvent_t4058973021_StaticFields
{
public:
	// System.String UnityEngine.Analytics.AnalyticsEvent::k_SdkVersion
	String_t* ___k_SdkVersion_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsEvent::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEvent::_debugMode
	bool ____debugMode_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Analytics.AnalyticsEvent::enumRenameTable
	Dictionary_2_t1632706988 * ___enumRenameTable_3;

public:
	inline static int32_t get_offset_of_k_SdkVersion_0() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___k_SdkVersion_0)); }
	inline String_t* get_k_SdkVersion_0() const { return ___k_SdkVersion_0; }
	inline String_t** get_address_of_k_SdkVersion_0() { return &___k_SdkVersion_0; }
	inline void set_k_SdkVersion_0(String_t* value)
	{
		___k_SdkVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SdkVersion_0), value);
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___m_EventData_1)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of_enumRenameTable_3() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___enumRenameTable_3)); }
	inline Dictionary_2_t1632706988 * get_enumRenameTable_3() const { return ___enumRenameTable_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_enumRenameTable_3() { return &___enumRenameTable_3; }
	inline void set_enumRenameTable_3(Dictionary_2_t1632706988 * value)
	{
		___enumRenameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___enumRenameTable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENT_T4058973021_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef U3CHIDEFLOORCOMU3EC__ANONSTOREY0_T4283171063_H
#define U3CHIDEFLOORCOMU3EC__ANONSTOREY0_T4283171063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.floorLogic.floorLogic/<HideFloorCom>c__AnonStorey0
struct  U3CHideFloorComU3Ec__AnonStorey0_t4283171063  : public RuntimeObject
{
public:
	// System.Int32 assets.floorLogic.floorLogic/<HideFloorCom>c__AnonStorey0::floorNumber
	int32_t ___floorNumber_0;
	// assets.floorLogic.floorLogic assets.floorLogic.floorLogic/<HideFloorCom>c__AnonStorey0::$this
	floorLogic_t3814374729 * ___U24this_1;

public:
	inline static int32_t get_offset_of_floorNumber_0() { return static_cast<int32_t>(offsetof(U3CHideFloorComU3Ec__AnonStorey0_t4283171063, ___floorNumber_0)); }
	inline int32_t get_floorNumber_0() const { return ___floorNumber_0; }
	inline int32_t* get_address_of_floorNumber_0() { return &___floorNumber_0; }
	inline void set_floorNumber_0(int32_t value)
	{
		___floorNumber_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CHideFloorComU3Ec__AnonStorey0_t4283171063, ___U24this_1)); }
	inline floorLogic_t3814374729 * get_U24this_1() const { return ___U24this_1; }
	inline floorLogic_t3814374729 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(floorLogic_t3814374729 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEFLOORCOMU3EC__ANONSTOREY0_T4283171063_H
#ifndef U3CCALCULATEPATHU3EC__ITERATOR0_T2361313786_H
#define U3CCALCULATEPATHU3EC__ITERATOR0_T2361313786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0
struct  U3CCalculatePathU3Ec__Iterator0_t2361313786  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_0;
	// System.String assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::url
	String_t* ___url_1;
	// assets.pathLogic.pathLogic assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::$this
	pathLogic_t3937024697 * ___U24this_2;
	// System.Object assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786, ___U3CformU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786, ___U24this_2)); }
	inline pathLogic_t3937024697 * get_U24this_2() const { return ___U24this_2; }
	inline pathLogic_t3937024697 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(pathLogic_t3937024697 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

struct U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields
{
public:
	// System.Predicate`1<System.String> assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::<>f__am$cache0
	Predicate_1_t2672744813 * ___U3CU3Ef__amU24cache0_6;
	// System.Predicate`1<System.String> assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::<>f__am$cache1
	Predicate_1_t2672744813 * ___U3CU3Ef__amU24cache1_7;
	// System.Predicate`1<System.String> assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::<>f__am$cache2
	Predicate_1_t2672744813 * ___U3CU3Ef__amU24cache2_8;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0::<>f__am$cache3
	Func_2_t4243939292 * ___U3CU3Ef__amU24cache3_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Predicate_1_t2672744813 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Predicate_1_t2672744813 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Predicate_1_t2672744813 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Predicate_1_t2672744813 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Predicate_1_t2672744813 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Predicate_1_t2672744813 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_8() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields, ___U3CU3Ef__amU24cache2_8)); }
	inline Predicate_1_t2672744813 * get_U3CU3Ef__amU24cache2_8() const { return ___U3CU3Ef__amU24cache2_8; }
	inline Predicate_1_t2672744813 ** get_address_of_U3CU3Ef__amU24cache2_8() { return &___U3CU3Ef__amU24cache2_8; }
	inline void set_U3CU3Ef__amU24cache2_8(Predicate_1_t2672744813 * value)
	{
		___U3CU3Ef__amU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_9() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields, ___U3CU3Ef__amU24cache3_9)); }
	inline Func_2_t4243939292 * get_U3CU3Ef__amU24cache3_9() const { return ___U3CU3Ef__amU24cache3_9; }
	inline Func_2_t4243939292 ** get_address_of_U3CU3Ef__amU24cache3_9() { return &___U3CU3Ef__amU24cache3_9; }
	inline void set_U3CU3Ef__amU24cache3_9(Func_2_t4243939292 * value)
	{
		___U3CU3Ef__amU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALCULATEPATHU3EC__ITERATOR0_T2361313786_H
#ifndef U3CCALCULATEPATHU3EC__ANONSTOREY1_T2938898595_H
#define U3CCALCULATEPATHU3EC__ANONSTOREY1_T2938898595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey1
struct  U3CCalculatePathU3Ec__AnonStorey1_t2938898595  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey1::j
	List_1_t3319525431 * ___j_0;
	// assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0 assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey1::<>f__ref$0
	U3CCalculatePathU3Ec__Iterator0_t2361313786 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_j_0() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__AnonStorey1_t2938898595, ___j_0)); }
	inline List_1_t3319525431 * get_j_0() const { return ___j_0; }
	inline List_1_t3319525431 ** get_address_of_j_0() { return &___j_0; }
	inline void set_j_0(List_1_t3319525431 * value)
	{
		___j_0 = value;
		Il2CppCodeGenWriteBarrier((&___j_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__AnonStorey1_t2938898595, ___U3CU3Ef__refU240_1)); }
	inline U3CCalculatePathU3Ec__Iterator0_t2361313786 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CCalculatePathU3Ec__Iterator0_t2361313786 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CCalculatePathU3Ec__Iterator0_t2361313786 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALCULATEPATHU3EC__ANONSTOREY1_T2938898595_H
#ifndef U3CCALCULATEPATHU3EC__ANONSTOREY2_T982583459_H
#define U3CCALCULATEPATHU3EC__ANONSTOREY2_T982583459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey2
struct  U3CCalculatePathU3Ec__AnonStorey2_t982583459  : public RuntimeObject
{
public:
	// System.Int32 assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey2::i
	int32_t ___i_0;
	// assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0 assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey2::<>f__ref$0
	U3CCalculatePathU3Ec__Iterator0_t2361313786 * ___U3CU3Ef__refU240_1;
	// assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey1 assets.pathLogic.pathLogic/<CalculatePath>c__Iterator0/<CalculatePath>c__AnonStorey2::<>f__ref$1
	U3CCalculatePathU3Ec__AnonStorey1_t2938898595 * ___U3CU3Ef__refU241_2;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__AnonStorey2_t982583459, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__AnonStorey2_t982583459, ___U3CU3Ef__refU240_1)); }
	inline U3CCalculatePathU3Ec__Iterator0_t2361313786 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CCalculatePathU3Ec__Iterator0_t2361313786 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CCalculatePathU3Ec__Iterator0_t2361313786 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_2() { return static_cast<int32_t>(offsetof(U3CCalculatePathU3Ec__AnonStorey2_t982583459, ___U3CU3Ef__refU241_2)); }
	inline U3CCalculatePathU3Ec__AnonStorey1_t2938898595 * get_U3CU3Ef__refU241_2() const { return ___U3CU3Ef__refU241_2; }
	inline U3CCalculatePathU3Ec__AnonStorey1_t2938898595 ** get_address_of_U3CU3Ef__refU241_2() { return &___U3CU3Ef__refU241_2; }
	inline void set_U3CU3Ef__refU241_2(U3CCalculatePathU3Ec__AnonStorey1_t2938898595 * value)
	{
		___U3CU3Ef__refU241_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALCULATEPATHU3EC__ANONSTOREY2_T982583459_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef U24ARRAYTYPEU3D40_T2865632059_H
#define U24ARRAYTYPEU3D40_T2865632059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_t2865632059 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_t2865632059__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_T2865632059_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef FONTASSETCREATIONSETTINGS_T359369028_H
#define FONTASSETCREATIONSETTINGS_T359369028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontAssetCreationSettings
struct  FontAssetCreationSettings_t359369028 
{
public:
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileName
	String_t* ___sourceFontFileName_0;
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileGUID
	String_t* ___sourceFontFileGUID_1;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSizeSamplingMode
	int32_t ___pointSizeSamplingMode_2;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSize
	int32_t ___pointSize_3;
	// System.Int32 TMPro.FontAssetCreationSettings::padding
	int32_t ___padding_4;
	// System.Int32 TMPro.FontAssetCreationSettings::packingMode
	int32_t ___packingMode_5;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasWidth
	int32_t ___atlasWidth_6;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasHeight
	int32_t ___atlasHeight_7;
	// System.Int32 TMPro.FontAssetCreationSettings::characterSetSelectionMode
	int32_t ___characterSetSelectionMode_8;
	// System.String TMPro.FontAssetCreationSettings::characterSequence
	String_t* ___characterSequence_9;
	// System.String TMPro.FontAssetCreationSettings::referencedFontAssetGUID
	String_t* ___referencedFontAssetGUID_10;
	// System.String TMPro.FontAssetCreationSettings::referencedTextAssetGUID
	String_t* ___referencedTextAssetGUID_11;
	// System.Int32 TMPro.FontAssetCreationSettings::fontStyle
	int32_t ___fontStyle_12;
	// System.Single TMPro.FontAssetCreationSettings::fontStyleModifier
	float ___fontStyleModifier_13;
	// System.Int32 TMPro.FontAssetCreationSettings::renderMode
	int32_t ___renderMode_14;
	// System.Boolean TMPro.FontAssetCreationSettings::includeFontFeatures
	bool ___includeFontFeatures_15;

public:
	inline static int32_t get_offset_of_sourceFontFileName_0() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___sourceFontFileName_0)); }
	inline String_t* get_sourceFontFileName_0() const { return ___sourceFontFileName_0; }
	inline String_t** get_address_of_sourceFontFileName_0() { return &___sourceFontFileName_0; }
	inline void set_sourceFontFileName_0(String_t* value)
	{
		___sourceFontFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileName_0), value);
	}

	inline static int32_t get_offset_of_sourceFontFileGUID_1() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___sourceFontFileGUID_1)); }
	inline String_t* get_sourceFontFileGUID_1() const { return ___sourceFontFileGUID_1; }
	inline String_t** get_address_of_sourceFontFileGUID_1() { return &___sourceFontFileGUID_1; }
	inline void set_sourceFontFileGUID_1(String_t* value)
	{
		___sourceFontFileGUID_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileGUID_1), value);
	}

	inline static int32_t get_offset_of_pointSizeSamplingMode_2() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___pointSizeSamplingMode_2)); }
	inline int32_t get_pointSizeSamplingMode_2() const { return ___pointSizeSamplingMode_2; }
	inline int32_t* get_address_of_pointSizeSamplingMode_2() { return &___pointSizeSamplingMode_2; }
	inline void set_pointSizeSamplingMode_2(int32_t value)
	{
		___pointSizeSamplingMode_2 = value;
	}

	inline static int32_t get_offset_of_pointSize_3() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___pointSize_3)); }
	inline int32_t get_pointSize_3() const { return ___pointSize_3; }
	inline int32_t* get_address_of_pointSize_3() { return &___pointSize_3; }
	inline void set_pointSize_3(int32_t value)
	{
		___pointSize_3 = value;
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___padding_4)); }
	inline int32_t get_padding_4() const { return ___padding_4; }
	inline int32_t* get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(int32_t value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_packingMode_5() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___packingMode_5)); }
	inline int32_t get_packingMode_5() const { return ___packingMode_5; }
	inline int32_t* get_address_of_packingMode_5() { return &___packingMode_5; }
	inline void set_packingMode_5(int32_t value)
	{
		___packingMode_5 = value;
	}

	inline static int32_t get_offset_of_atlasWidth_6() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___atlasWidth_6)); }
	inline int32_t get_atlasWidth_6() const { return ___atlasWidth_6; }
	inline int32_t* get_address_of_atlasWidth_6() { return &___atlasWidth_6; }
	inline void set_atlasWidth_6(int32_t value)
	{
		___atlasWidth_6 = value;
	}

	inline static int32_t get_offset_of_atlasHeight_7() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___atlasHeight_7)); }
	inline int32_t get_atlasHeight_7() const { return ___atlasHeight_7; }
	inline int32_t* get_address_of_atlasHeight_7() { return &___atlasHeight_7; }
	inline void set_atlasHeight_7(int32_t value)
	{
		___atlasHeight_7 = value;
	}

	inline static int32_t get_offset_of_characterSetSelectionMode_8() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___characterSetSelectionMode_8)); }
	inline int32_t get_characterSetSelectionMode_8() const { return ___characterSetSelectionMode_8; }
	inline int32_t* get_address_of_characterSetSelectionMode_8() { return &___characterSetSelectionMode_8; }
	inline void set_characterSetSelectionMode_8(int32_t value)
	{
		___characterSetSelectionMode_8 = value;
	}

	inline static int32_t get_offset_of_characterSequence_9() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___characterSequence_9)); }
	inline String_t* get_characterSequence_9() const { return ___characterSequence_9; }
	inline String_t** get_address_of_characterSequence_9() { return &___characterSequence_9; }
	inline void set_characterSequence_9(String_t* value)
	{
		___characterSequence_9 = value;
		Il2CppCodeGenWriteBarrier((&___characterSequence_9), value);
	}

	inline static int32_t get_offset_of_referencedFontAssetGUID_10() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___referencedFontAssetGUID_10)); }
	inline String_t* get_referencedFontAssetGUID_10() const { return ___referencedFontAssetGUID_10; }
	inline String_t** get_address_of_referencedFontAssetGUID_10() { return &___referencedFontAssetGUID_10; }
	inline void set_referencedFontAssetGUID_10(String_t* value)
	{
		___referencedFontAssetGUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___referencedFontAssetGUID_10), value);
	}

	inline static int32_t get_offset_of_referencedTextAssetGUID_11() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___referencedTextAssetGUID_11)); }
	inline String_t* get_referencedTextAssetGUID_11() const { return ___referencedTextAssetGUID_11; }
	inline String_t** get_address_of_referencedTextAssetGUID_11() { return &___referencedTextAssetGUID_11; }
	inline void set_referencedTextAssetGUID_11(String_t* value)
	{
		___referencedTextAssetGUID_11 = value;
		Il2CppCodeGenWriteBarrier((&___referencedTextAssetGUID_11), value);
	}

	inline static int32_t get_offset_of_fontStyle_12() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___fontStyle_12)); }
	inline int32_t get_fontStyle_12() const { return ___fontStyle_12; }
	inline int32_t* get_address_of_fontStyle_12() { return &___fontStyle_12; }
	inline void set_fontStyle_12(int32_t value)
	{
		___fontStyle_12 = value;
	}

	inline static int32_t get_offset_of_fontStyleModifier_13() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___fontStyleModifier_13)); }
	inline float get_fontStyleModifier_13() const { return ___fontStyleModifier_13; }
	inline float* get_address_of_fontStyleModifier_13() { return &___fontStyleModifier_13; }
	inline void set_fontStyleModifier_13(float value)
	{
		___fontStyleModifier_13 = value;
	}

	inline static int32_t get_offset_of_renderMode_14() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___renderMode_14)); }
	inline int32_t get_renderMode_14() const { return ___renderMode_14; }
	inline int32_t* get_address_of_renderMode_14() { return &___renderMode_14; }
	inline void set_renderMode_14(int32_t value)
	{
		___renderMode_14 = value;
	}

	inline static int32_t get_offset_of_includeFontFeatures_15() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___includeFontFeatures_15)); }
	inline bool get_includeFontFeatures_15() const { return ___includeFontFeatures_15; }
	inline bool* get_address_of_includeFontFeatures_15() { return &___includeFontFeatures_15; }
	inline void set_includeFontFeatures_15(bool value)
	{
		___includeFontFeatures_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_t359369028_marshaled_pinvoke
{
	char* ___sourceFontFileName_0;
	char* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	char* ___characterSequence_9;
	char* ___referencedFontAssetGUID_10;
	char* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
// Native definition for COM marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_t359369028_marshaled_com
{
	Il2CppChar* ___sourceFontFileName_0;
	Il2CppChar* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	Il2CppChar* ___characterSequence_9;
	Il2CppChar* ___referencedFontAssetGUID_10;
	Il2CppChar* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
#endif // FONTASSETCREATIONSETTINGS_T359369028_H
#ifndef GLYPHVALUERECORD_T4065874512_H
#define GLYPHVALUERECORD_T4065874512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.GlyphValueRecord
struct  GlyphValueRecord_t4065874512 
{
public:
	// System.Single TMPro.GlyphValueRecord::xPlacement
	float ___xPlacement_0;
	// System.Single TMPro.GlyphValueRecord::yPlacement
	float ___yPlacement_1;
	// System.Single TMPro.GlyphValueRecord::xAdvance
	float ___xAdvance_2;
	// System.Single TMPro.GlyphValueRecord::yAdvance
	float ___yAdvance_3;

public:
	inline static int32_t get_offset_of_xPlacement_0() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___xPlacement_0)); }
	inline float get_xPlacement_0() const { return ___xPlacement_0; }
	inline float* get_address_of_xPlacement_0() { return &___xPlacement_0; }
	inline void set_xPlacement_0(float value)
	{
		___xPlacement_0 = value;
	}

	inline static int32_t get_offset_of_yPlacement_1() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___yPlacement_1)); }
	inline float get_yPlacement_1() const { return ___yPlacement_1; }
	inline float* get_address_of_yPlacement_1() { return &___yPlacement_1; }
	inline void set_yPlacement_1(float value)
	{
		___yPlacement_1 = value;
	}

	inline static int32_t get_offset_of_xAdvance_2() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___xAdvance_2)); }
	inline float get_xAdvance_2() const { return ___xAdvance_2; }
	inline float* get_address_of_xAdvance_2() { return &___xAdvance_2; }
	inline void set_xAdvance_2(float value)
	{
		___xAdvance_2 = value;
	}

	inline static int32_t get_offset_of_yAdvance_3() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___yAdvance_3)); }
	inline float get_yAdvance_3() const { return ___yAdvance_3; }
	inline float* get_address_of_yAdvance_3() { return &___yAdvance_3; }
	inline void set_yAdvance_3(float value)
	{
		___yAdvance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHVALUERECORD_T4065874512_H
#ifndef KERNINGPAIRKEY_T536493877_H
#define KERNINGPAIRKEY_T536493877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPairKey
struct  KerningPairKey_t536493877 
{
public:
	// System.UInt32 TMPro.KerningPairKey::ascii_Left
	uint32_t ___ascii_Left_0;
	// System.UInt32 TMPro.KerningPairKey::ascii_Right
	uint32_t ___ascii_Right_1;
	// System.UInt32 TMPro.KerningPairKey::key
	uint32_t ___key_2;

public:
	inline static int32_t get_offset_of_ascii_Left_0() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Left_0)); }
	inline uint32_t get_ascii_Left_0() const { return ___ascii_Left_0; }
	inline uint32_t* get_address_of_ascii_Left_0() { return &___ascii_Left_0; }
	inline void set_ascii_Left_0(uint32_t value)
	{
		___ascii_Left_0 = value;
	}

	inline static int32_t get_offset_of_ascii_Right_1() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Right_1)); }
	inline uint32_t get_ascii_Right_1() const { return ___ascii_Right_1; }
	inline uint32_t* get_address_of_ascii_Right_1() { return &___ascii_Right_1; }
	inline void set_ascii_Right_1(uint32_t value)
	{
		___ascii_Right_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___key_2)); }
	inline uint32_t get_key_2() const { return ___key_2; }
	inline uint32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(uint32_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIRKEY_T536493877_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef TMP_BASICXMLTAGSTACK_T2962628096_H
#define TMP_BASICXMLTAGSTACK_T2962628096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t2962628096 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T2962628096_H
#ifndef TMP_LINKINFO_T1092083476_H
#define TMP_LINKINFO_T1092083476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t1092083476 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t3528271667* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkID_6)); }
	inline CharU5BU5D_t3528271667* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t3528271667** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t3528271667* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T1092083476_H
#ifndef TMP_PAGEINFO_T2608430633_H
#define TMP_PAGEINFO_T2608430633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t2608430633 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T2608430633_H
#ifndef TMP_SPRITEINFO_T2726321384_H
#define TMP_SPRITEINFO_T2726321384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t2726321384 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T2726321384_H
#ifndef TMP_WORDINFO_T3331066303_H
#define TMP_WORDINFO_T3331066303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t3331066303 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T3331066303_H
#ifndef TMP_XMLTAGSTACK_1_T2514600297_H
#define TMP_XMLTAGSTACK_1_T2514600297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2514600297 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t385246372* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___itemStack_0)); }
	inline Int32U5BU5D_t385246372* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t385246372* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2514600297_H
#ifndef TMP_XMLTAGSTACK_1_T960921318_H
#define TMP_XMLTAGSTACK_1_T960921318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t960921318 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t1444911251* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___itemStack_0)); }
	inline SingleU5BU5D_t1444911251* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t1444911251* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T960921318_H
#ifndef TMP_XMLTAGSTACK_1_T3241710312_H
#define TMP_XMLTAGSTACK_1_T3241710312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t3241710312 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2496920137* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_t3678055768 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t2496920137* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t2496920137** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t2496920137* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_t3678055768 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_t3678055768 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3241710312_H
#ifndef TAGATTRIBUTE_T688278634_H
#define TAGATTRIBUTE_T688278634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t688278634 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T688278634_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-9E6378168821DBABB7EE3D0154346480FAC8AEF1
	U24ArrayTypeU3D40_t2865632059  ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline U24ArrayTypeU3D40_t2865632059  get_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline U24ArrayTypeU3D40_t2865632059 * get_address_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(U24ArrayTypeU3D40_t2865632059  value)
	{
		___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1520811813_H
#define U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1520811813  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>c__Iterator0::<matrix>__0
	Matrix4x4_t1817901843  ___U3CmatrixU3E__0_0;
	// EnvMapAnimator EnvMapAnimator/<Start>c__Iterator0::$this
	EnvMapAnimator_t1140999784 * ___U24this_1;
	// System.Object EnvMapAnimator/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EnvMapAnimator/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EnvMapAnimator/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmatrixU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U3CmatrixU3E__0_0)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__0_0() const { return ___U3CmatrixU3E__0_0; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__0_0() { return &___U3CmatrixU3E__0_0; }
	inline void set_U3CmatrixU3E__0_0(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24this_1)); }
	inline EnvMapAnimator_t1140999784 * get_U24this_1() const { return ___U24this_1; }
	inline EnvMapAnimator_t1140999784 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EnvMapAnimator_t1140999784 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef CAMERAMODES_T3200559075_H
#define CAMERAMODES_T3200559075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController/CameraModes
struct  CameraModes_t3200559075 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraModes_t3200559075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T3200559075_H
#ifndef MOTIONTYPE_T1905163921_H
#define MOTIONTYPE_T1905163921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin/MotionType
struct  MotionType_t1905163921 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionType_t1905163921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T1905163921_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#define U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t116130919  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_ShearValue>__0
	float ___U3Cold_ShearValueU3E__0_1;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3046754366 * ___U3Cold_curveU3E__0_2;
	// TMPro.TMP_TextInfo TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__1_3;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_5;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_6;
	// UnityEngine.Vector3[] TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E__2_7;
	// UnityEngine.Matrix4x4 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_8;
	// TMPro.Examples.SkewTextExample TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$this
	SkewTextExample_t3460249701 * ___U24this_9;
	// System.Object TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_ShearValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_ShearValueU3E__0_1)); }
	inline float get_U3Cold_ShearValueU3E__0_1() const { return ___U3Cold_ShearValueU3E__0_1; }
	inline float* get_address_of_U3Cold_ShearValueU3E__0_1() { return &___U3Cold_ShearValueU3E__0_1; }
	inline void set_U3Cold_ShearValueU3E__0_1(float value)
	{
		___U3Cold_ShearValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_curveU3E__0_2)); }
	inline AnimationCurve_t3046754366 * get_U3Cold_curveU3E__0_2() const { return ___U3Cold_curveU3E__0_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3Cold_curveU3E__0_2() { return &___U3Cold_curveU3E__0_2; }
	inline void set_U3Cold_curveU3E__0_2(AnimationCurve_t3046754366 * value)
	{
		___U3Cold_curveU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CtextInfoU3E__1_3)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__1_3() const { return ___U3CtextInfoU3E__1_3; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__1_3() { return &___U3CtextInfoU3E__1_3; }
	inline void set_U3CtextInfoU3E__1_3(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMinXU3E__1_5)); }
	inline float get_U3CboundsMinXU3E__1_5() const { return ___U3CboundsMinXU3E__1_5; }
	inline float* get_address_of_U3CboundsMinXU3E__1_5() { return &___U3CboundsMinXU3E__1_5; }
	inline void set_U3CboundsMinXU3E__1_5(float value)
	{
		___U3CboundsMinXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMaxXU3E__1_6)); }
	inline float get_U3CboundsMaxXU3E__1_6() const { return ___U3CboundsMaxXU3E__1_6; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_6() { return &___U3CboundsMaxXU3E__1_6; }
	inline void set_U3CboundsMaxXU3E__1_6(float value)
	{
		___U3CboundsMaxXU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CverticesU3E__2_7)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E__2_7() const { return ___U3CverticesU3E__2_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E__2_7() { return &___U3CverticesU3E__2_7; }
	inline void set_U3CverticesU3E__2_7(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CmatrixU3E__2_8)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_8() const { return ___U3CmatrixU3E__2_8; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_8() { return &___U3CmatrixU3E__2_8; }
	inline void set_U3CmatrixU3E__2_8(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24this_9)); }
	inline SkewTextExample_t3460249701 * get_U24this_9() const { return ___U24this_9; }
	inline SkewTextExample_t3460249701 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(SkewTextExample_t3460249701 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifndef OBJECTTYPE_T4082700821_H
#define OBJECTTYPE_T4082700821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01/objectType
struct  objectType_t4082700821 
{
public:
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01/objectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(objectType_t4082700821, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPE_T4082700821_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#define FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t1585798158 
{
public:
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t1585798158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifndef U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#define U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1
struct  U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$this
	TextMeshProFloatingText_t845872552 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24this_8)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifndef U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#define U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0
struct  U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$this
	TextMeshProFloatingText_t845872552 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24this_8)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef KERNINGPAIR_T2270855589_H
#define KERNINGPAIR_T2270855589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPair
struct  KerningPair_t2270855589  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningPair::m_FirstGlyph
	uint32_t ___m_FirstGlyph_0;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_FirstGlyphAdjustments
	GlyphValueRecord_t4065874512  ___m_FirstGlyphAdjustments_1;
	// System.UInt32 TMPro.KerningPair::m_SecondGlyph
	uint32_t ___m_SecondGlyph_2;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_SecondGlyphAdjustments
	GlyphValueRecord_t4065874512  ___m_SecondGlyphAdjustments_3;
	// System.Single TMPro.KerningPair::xOffset
	float ___xOffset_4;

public:
	inline static int32_t get_offset_of_m_FirstGlyph_0() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_FirstGlyph_0)); }
	inline uint32_t get_m_FirstGlyph_0() const { return ___m_FirstGlyph_0; }
	inline uint32_t* get_address_of_m_FirstGlyph_0() { return &___m_FirstGlyph_0; }
	inline void set_m_FirstGlyph_0(uint32_t value)
	{
		___m_FirstGlyph_0 = value;
	}

	inline static int32_t get_offset_of_m_FirstGlyphAdjustments_1() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_FirstGlyphAdjustments_1)); }
	inline GlyphValueRecord_t4065874512  get_m_FirstGlyphAdjustments_1() const { return ___m_FirstGlyphAdjustments_1; }
	inline GlyphValueRecord_t4065874512 * get_address_of_m_FirstGlyphAdjustments_1() { return &___m_FirstGlyphAdjustments_1; }
	inline void set_m_FirstGlyphAdjustments_1(GlyphValueRecord_t4065874512  value)
	{
		___m_FirstGlyphAdjustments_1 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyph_2() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_SecondGlyph_2)); }
	inline uint32_t get_m_SecondGlyph_2() const { return ___m_SecondGlyph_2; }
	inline uint32_t* get_address_of_m_SecondGlyph_2() { return &___m_SecondGlyph_2; }
	inline void set_m_SecondGlyph_2(uint32_t value)
	{
		___m_SecondGlyph_2 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyphAdjustments_3() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_SecondGlyphAdjustments_3)); }
	inline GlyphValueRecord_t4065874512  get_m_SecondGlyphAdjustments_3() const { return ___m_SecondGlyphAdjustments_3; }
	inline GlyphValueRecord_t4065874512 * get_address_of_m_SecondGlyphAdjustments_3() { return &___m_SecondGlyphAdjustments_3; }
	inline void set_m_SecondGlyphAdjustments_3(GlyphValueRecord_t4065874512  value)
	{
		___m_SecondGlyphAdjustments_3 = value;
	}

	inline static int32_t get_offset_of_xOffset_4() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___xOffset_4)); }
	inline float get_xOffset_4() const { return ___xOffset_4; }
	inline float* get_address_of_xOffset_4() { return &___xOffset_4; }
	inline void set_xOffset_4(float value)
	{
		___xOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIR_T2270855589_H
#ifndef MESH_EXTENTS_T3388355125_H
#define MESH_EXTENTS_T3388355125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t3388355125 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T3388355125_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef TMP_VERTEX_T2404176824_H
#define TMP_VERTEX_T2404176824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t2404176824 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2156229523  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2156229523  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2156229523  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t2600501292  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv_1)); }
	inline Vector2_t2156229523  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2156229523 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2156229523  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv2_2)); }
	inline Vector2_t2156229523  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2156229523 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2156229523  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv4_3)); }
	inline Vector2_t2156229523  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2156229523 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2156229523  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___color_4)); }
	inline Color32_t2600501292  get_color_4() const { return ___color_4; }
	inline Color32_t2600501292 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t2600501292  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T2404176824_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#define TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_t388000256 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_t388000256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifndef TMP_XMLTAGSTACK_1_T1515999176_H
#define TMP_XMLTAGSTACK_1_T1515999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t1515999176 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t648826345* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t1952344632  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t648826345* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t648826345* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_defaultItem_3)); }
	inline MaterialReference_t1952344632  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t1952344632 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t1952344632  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1515999176_H
#ifndef TMP_XMLTAGSTACK_1_T2164155836_H
#define TMP_XMLTAGSTACK_1_T2164155836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t2164155836 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t3850468773* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t2600501292  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___itemStack_0)); }
	inline Color32U5BU5D_t3850468773* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t3850468773* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_defaultItem_3)); }
	inline Color32_t2600501292  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t2600501292 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t2600501292  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2164155836_H
#ifndef TAGTYPE_T123236451_H
#define TAGTYPE_T123236451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t123236451 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t123236451, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T123236451_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef VERTEXGRADIENT_T345148380_H
#define VERTEXGRADIENT_T345148380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t345148380 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2555686324  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2555686324  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2555686324  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topLeft_0)); }
	inline Color_t2555686324  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2555686324 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2555686324  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topRight_1)); }
	inline Color_t2555686324  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2555686324 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2555686324  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomLeft_2)); }
	inline Color_t2555686324  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2555686324 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2555686324  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomRight_3)); }
	inline Color_t2555686324  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2555686324 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2555686324  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T345148380_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TMP_CHARACTERINFO_T3185626797_H
#define TMP_CHARACTERINFO_T3185626797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t3185626797 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t129727469 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t340375123 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t3722313464  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t3722313464  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t3722313464  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t3722313464  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t2600501292  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___textElement_3)); }
	inline TMP_TextElement_t129727469 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t129727469 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t129727469 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___fontAsset_4)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pageNumber_12)); }
	inline int32_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int32_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int32_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TL_14)); }
	inline TMP_Vertex_t2404176824  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t2404176824  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BL_15)); }
	inline TMP_Vertex_t2404176824  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t2404176824  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TR_16)); }
	inline TMP_Vertex_t2404176824  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t2404176824  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BR_17)); }
	inline TMP_Vertex_t2404176824  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t2404176824  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topLeft_18)); }
	inline Vector3_t3722313464  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t3722313464 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t3722313464  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomLeft_19)); }
	inline Vector3_t3722313464  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t3722313464  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topRight_20)); }
	inline Vector3_t3722313464  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t3722313464 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t3722313464  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomRight_21)); }
	inline Vector3_t3722313464  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t3722313464  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___color_29)); }
	inline Color32_t2600501292  get_color_29() const { return ___color_29; }
	inline Color32_t2600501292 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t2600501292  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T3185626797_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_19)); }
	inline Extents_t3837212874  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_t3837212874 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_t3837212874  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef TMP_XMLTAGSTACK_1_T3600445780_H
#define TMP_XMLTAGSTACK_1_T3600445780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t3600445780 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t3552942253* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t3552942253* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t3552942253** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t3552942253* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3600445780_H
#ifndef XML_TAGATTRIBUTE_T1174424309_H
#define XML_TAGATTRIBUTE_T1174424309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_t1174424309 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// TMPro.TagType TMPro.XML_TagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueType_1)); }
	inline int32_t get_valueType_1() const { return ___valueType_1; }
	inline int32_t* get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(int32_t value)
	{
		___valueType_1 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueStartIndex_2)); }
	inline int32_t get_valueStartIndex_2() const { return ___valueStartIndex_2; }
	inline int32_t* get_address_of_valueStartIndex_2() { return &___valueStartIndex_2; }
	inline void set_valueStartIndex_2(int32_t value)
	{
		___valueStartIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XML_TAGATTRIBUTE_T1174424309_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef TMP_INPUTVALIDATOR_T1385053824_H
#define TMP_INPUTVALIDATOR_T1385053824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t1385053824  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T1385053824_H
#ifndef WORDWRAPSTATE_T341939652_H
#define WORDWRAPSTATE_T341939652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t3241710312  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t3241710312  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_50)); }
	inline Material_t340375123 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_t340375123 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_t340375123 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_52)); }
	inline Extents_t3837212874  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_t3837212874 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_t3837212874  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T341939652_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TMP_DIGITVALIDATOR_T573672104_H
#define TMP_DIGITVALIDATOR_T573672104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DigitValidator
struct  TMP_DigitValidator_t573672104  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DIGITVALIDATOR_T573672104_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CHATCONTROLLER_T3486202795_H
#define CHATCONTROLLER_T3486202795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t3486202795  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_t1099764886 * ___TMP_ChatInput_4;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t2599618874 * ___TMP_ChatOutput_5;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t1494447233 * ___ChatScrollbar_6;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_4() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatInput_4)); }
	inline TMP_InputField_t1099764886 * get_TMP_ChatInput_4() const { return ___TMP_ChatInput_4; }
	inline TMP_InputField_t1099764886 ** get_address_of_TMP_ChatInput_4() { return &___TMP_ChatInput_4; }
	inline void set_TMP_ChatInput_4(TMP_InputField_t1099764886 * value)
	{
		___TMP_ChatInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_4), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_5() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatOutput_5)); }
	inline TMP_Text_t2599618874 * get_TMP_ChatOutput_5() const { return ___TMP_ChatOutput_5; }
	inline TMP_Text_t2599618874 ** get_address_of_TMP_ChatOutput_5() { return &___TMP_ChatOutput_5; }
	inline void set_TMP_ChatOutput_5(TMP_Text_t2599618874 * value)
	{
		___TMP_ChatOutput_5 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_5), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_6() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___ChatScrollbar_6)); }
	inline Scrollbar_t1494447233 * get_ChatScrollbar_6() const { return ___ChatScrollbar_6; }
	inline Scrollbar_t1494447233 ** get_address_of_ChatScrollbar_6() { return &___ChatScrollbar_6; }
	inline void set_ChatScrollbar_6(Scrollbar_t1494447233 * value)
	{
		___ChatScrollbar_6 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T3486202795_H
#ifndef ENVMAPANIMATOR_T1140999784_H
#define ENVMAPANIMATOR_T1140999784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1140999784  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t3722313464  ___RotationSpeeds_4;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_5;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t340375123 * ___m_material_6;

public:
	inline static int32_t get_offset_of_RotationSpeeds_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___RotationSpeeds_4)); }
	inline Vector3_t3722313464  get_RotationSpeeds_4() const { return ___RotationSpeeds_4; }
	inline Vector3_t3722313464 * get_address_of_RotationSpeeds_4() { return &___RotationSpeeds_4; }
	inline void set_RotationSpeeds_4(Vector3_t3722313464  value)
	{
		___RotationSpeeds_4 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_textMeshPro_5)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_material_6() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_material_6)); }
	inline Material_t340375123 * get_m_material_6() const { return ___m_material_6; }
	inline Material_t340375123 ** get_address_of_m_material_6() { return &___m_material_6; }
	inline void set_m_material_6(Material_t340375123 * value)
	{
		___m_material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T1140999784_H
#ifndef LEGENDBEHAVIOUR_T178662842_H
#define LEGENDBEHAVIOUR_T178662842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LegendBehaviour
struct  LegendBehaviour_t178662842  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LegendBehaviour::panel
	GameObject_t1113636619 * ___panel_4;
	// UnityEngine.RectTransform LegendBehaviour::panel_transform
	RectTransform_t3704657025 * ___panel_transform_5;
	// System.Int32 LegendBehaviour::panel_height
	int32_t ___panel_height_6;
	// System.Int32 LegendBehaviour::panel_width
	int32_t ___panel_width_7;
	// System.Int32 LegendBehaviour::panel_pos_x
	int32_t ___panel_pos_x_8;
	// System.Int32 LegendBehaviour::panel_pos_y
	int32_t ___panel_pos_y_9;
	// UnityEngine.GameObject LegendBehaviour::buttonPrefab
	GameObject_t1113636619 * ___buttonPrefab_10;
	// UnityEngine.Font LegendBehaviour::font
	Font_t1956802104 * ___font_11;
	// System.Collections.Generic.List`1<System.String> LegendBehaviour::textList
	List_1_t3319525431 * ___textList_12;
	// System.Collections.Generic.List`1<System.String> LegendBehaviour::sprites
	List_1_t3319525431 * ___sprites_13;

public:
	inline static int32_t get_offset_of_panel_4() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___panel_4)); }
	inline GameObject_t1113636619 * get_panel_4() const { return ___panel_4; }
	inline GameObject_t1113636619 ** get_address_of_panel_4() { return &___panel_4; }
	inline void set_panel_4(GameObject_t1113636619 * value)
	{
		___panel_4 = value;
		Il2CppCodeGenWriteBarrier((&___panel_4), value);
	}

	inline static int32_t get_offset_of_panel_transform_5() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___panel_transform_5)); }
	inline RectTransform_t3704657025 * get_panel_transform_5() const { return ___panel_transform_5; }
	inline RectTransform_t3704657025 ** get_address_of_panel_transform_5() { return &___panel_transform_5; }
	inline void set_panel_transform_5(RectTransform_t3704657025 * value)
	{
		___panel_transform_5 = value;
		Il2CppCodeGenWriteBarrier((&___panel_transform_5), value);
	}

	inline static int32_t get_offset_of_panel_height_6() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___panel_height_6)); }
	inline int32_t get_panel_height_6() const { return ___panel_height_6; }
	inline int32_t* get_address_of_panel_height_6() { return &___panel_height_6; }
	inline void set_panel_height_6(int32_t value)
	{
		___panel_height_6 = value;
	}

	inline static int32_t get_offset_of_panel_width_7() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___panel_width_7)); }
	inline int32_t get_panel_width_7() const { return ___panel_width_7; }
	inline int32_t* get_address_of_panel_width_7() { return &___panel_width_7; }
	inline void set_panel_width_7(int32_t value)
	{
		___panel_width_7 = value;
	}

	inline static int32_t get_offset_of_panel_pos_x_8() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___panel_pos_x_8)); }
	inline int32_t get_panel_pos_x_8() const { return ___panel_pos_x_8; }
	inline int32_t* get_address_of_panel_pos_x_8() { return &___panel_pos_x_8; }
	inline void set_panel_pos_x_8(int32_t value)
	{
		___panel_pos_x_8 = value;
	}

	inline static int32_t get_offset_of_panel_pos_y_9() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___panel_pos_y_9)); }
	inline int32_t get_panel_pos_y_9() const { return ___panel_pos_y_9; }
	inline int32_t* get_address_of_panel_pos_y_9() { return &___panel_pos_y_9; }
	inline void set_panel_pos_y_9(int32_t value)
	{
		___panel_pos_y_9 = value;
	}

	inline static int32_t get_offset_of_buttonPrefab_10() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___buttonPrefab_10)); }
	inline GameObject_t1113636619 * get_buttonPrefab_10() const { return ___buttonPrefab_10; }
	inline GameObject_t1113636619 ** get_address_of_buttonPrefab_10() { return &___buttonPrefab_10; }
	inline void set_buttonPrefab_10(GameObject_t1113636619 * value)
	{
		___buttonPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___buttonPrefab_10), value);
	}

	inline static int32_t get_offset_of_font_11() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___font_11)); }
	inline Font_t1956802104 * get_font_11() const { return ___font_11; }
	inline Font_t1956802104 ** get_address_of_font_11() { return &___font_11; }
	inline void set_font_11(Font_t1956802104 * value)
	{
		___font_11 = value;
		Il2CppCodeGenWriteBarrier((&___font_11), value);
	}

	inline static int32_t get_offset_of_textList_12() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___textList_12)); }
	inline List_1_t3319525431 * get_textList_12() const { return ___textList_12; }
	inline List_1_t3319525431 ** get_address_of_textList_12() { return &___textList_12; }
	inline void set_textList_12(List_1_t3319525431 * value)
	{
		___textList_12 = value;
		Il2CppCodeGenWriteBarrier((&___textList_12), value);
	}

	inline static int32_t get_offset_of_sprites_13() { return static_cast<int32_t>(offsetof(LegendBehaviour_t178662842, ___sprites_13)); }
	inline List_1_t3319525431 * get_sprites_13() const { return ___sprites_13; }
	inline List_1_t3319525431 ** get_address_of_sprites_13() { return &___sprites_13; }
	inline void set_sprites_13(List_1_t3319525431 * value)
	{
		___sprites_13 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGENDBEHAVIOUR_T178662842_H
#ifndef BENCHMARK01_T1571072624_H
#define BENCHMARK01_T1571072624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t1571072624  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_4;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_5;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_6;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_7;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_8;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t340375123 * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t340375123 * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_TMProFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TMProFont_5)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_5() const { return ___TMProFont_5; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_5() { return &___TMProFont_5; }
	inline void set_TMProFont_5(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_5), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TextMeshFont_6)); }
	inline Font_t1956802104 * get_TextMeshFont_6() const { return ___TextMeshFont_6; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_6() { return &___TextMeshFont_6; }
	inline void set_TextMeshFont_6(Font_t1956802104 * value)
	{
		___TextMeshFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_6), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_7() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMeshPro_7)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_7() const { return ___m_textMeshPro_7; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_7() { return &___m_textMeshPro_7; }
	inline void set_m_textMeshPro_7(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_7), value);
	}

	inline static int32_t get_offset_of_m_textContainer_8() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textContainer_8)); }
	inline TextContainer_t97923372 * get_m_textContainer_8() const { return ___m_textContainer_8; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_8() { return &___m_textContainer_8; }
	inline void set_m_textContainer_8(TextContainer_t97923372 * value)
	{
		___m_textContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_8), value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMesh_9)); }
	inline TextMesh_t1536577757 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(TextMesh_t1536577757 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_9), value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material01_12)); }
	inline Material_t340375123 * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_t340375123 ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_t340375123 * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_12), value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material02_13)); }
	inline Material_t340375123 * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_t340375123 ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_t340375123 * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T1571072624_H
#ifndef BENCHMARK01_UGUI_T3264177817_H
#define BENCHMARK01_UGUI_T3264177817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3264177817  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_4;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t3310196443 * ___canvas_5;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_6;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_7;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t529313277 * ___m_textMeshPro_8;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t1901882714 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t340375123 * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t340375123 * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_canvas_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___canvas_5)); }
	inline Canvas_t3310196443 * get_canvas_5() const { return ___canvas_5; }
	inline Canvas_t3310196443 ** get_address_of_canvas_5() { return &___canvas_5; }
	inline void set_canvas_5(Canvas_t3310196443 * value)
	{
		___canvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_5), value);
	}

	inline static int32_t get_offset_of_TMProFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TMProFont_6)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_6() const { return ___TMProFont_6; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_6() { return &___TMProFont_6; }
	inline void set_TMProFont_6(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_6), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TextMeshFont_7)); }
	inline Font_t1956802104 * get_TextMeshFont_7() const { return ___TextMeshFont_7; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_7() { return &___TextMeshFont_7; }
	inline void set_TextMeshFont_7(Font_t1956802104 * value)
	{
		___TextMeshFont_7 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_7), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_8() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMeshPro_8)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMeshPro_8() const { return ___m_textMeshPro_8; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMeshPro_8() { return &___m_textMeshPro_8; }
	inline void set_m_textMeshPro_8(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMesh_9)); }
	inline Text_t1901882714 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline Text_t1901882714 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(Text_t1901882714 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_9), value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material01_12)); }
	inline Material_t340375123 * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_t340375123 ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_t340375123 * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_12), value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material02_13)); }
	inline Material_t340375123 * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_t340375123 ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_t340375123 * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3264177817_H
#ifndef BENCHMARK02_T1571269232_H
#define BENCHMARK02_T1571269232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t1571269232  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_6() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___floatingText_Script_6)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_6() const { return ___floatingText_Script_6; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_6() { return &___floatingText_Script_6; }
	inline void set_floatingText_Script_6(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_6 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T1571269232_H
#ifndef BENCHMARK03_T1571203696_H
#define BENCHMARK03_T1571203696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1571203696  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t1956802104 * ___TheFont_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___TheFont_6)); }
	inline Font_t1956802104 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_t1956802104 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_t1956802104 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1571203696_H
#ifndef BENCHMARK04_T1570876016_H
#define BENCHMARK04_T1570876016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t1570876016  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_5;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_6;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_7;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_t3600365921 * ___m_Transform_8;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_5() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MinPointSize_5)); }
	inline int32_t get_MinPointSize_5() const { return ___MinPointSize_5; }
	inline int32_t* get_address_of_MinPointSize_5() { return &___MinPointSize_5; }
	inline void set_MinPointSize_5(int32_t value)
	{
		___MinPointSize_5 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_6() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MaxPointSize_6)); }
	inline int32_t get_MaxPointSize_6() const { return ___MaxPointSize_6; }
	inline int32_t* get_address_of_MaxPointSize_6() { return &___MaxPointSize_6; }
	inline void set_MaxPointSize_6(int32_t value)
	{
		___MaxPointSize_6 = value;
	}

	inline static int32_t get_offset_of_Steps_7() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___Steps_7)); }
	inline int32_t get_Steps_7() const { return ___Steps_7; }
	inline int32_t* get_address_of_Steps_7() { return &___Steps_7; }
	inline void set_Steps_7(int32_t value)
	{
		___Steps_7 = value;
	}

	inline static int32_t get_offset_of_m_Transform_8() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___m_Transform_8)); }
	inline Transform_t3600365921 * get_m_Transform_8() const { return ___m_Transform_8; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_8() { return &___m_Transform_8; }
	inline void set_m_Transform_8(Transform_t3600365921 * value)
	{
		___m_Transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T1570876016_H
#ifndef CAMERACONTROLLER_T2264742161_H
#define CAMERACONTROLLER_T2264742161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t2264742161  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_t3600365921 * ___cameraTransform_4;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_t3600365921 * ___dummyTarget_5;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_t3600365921 * ___CameraTarget_6;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_7;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_8;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_9;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_11;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_12;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_13;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_14;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_15;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_16;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_17;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_18;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_19;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_20;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t3722313464  ___currentVelocity_21;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t3722313464  ___desiredPosition_22;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_23;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_24;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t3722313464  ___moveVector_25;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_26;

public:
	inline static int32_t get_offset_of_cameraTransform_4() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___cameraTransform_4)); }
	inline Transform_t3600365921 * get_cameraTransform_4() const { return ___cameraTransform_4; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_4() { return &___cameraTransform_4; }
	inline void set_cameraTransform_4(Transform_t3600365921 * value)
	{
		___cameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_4), value);
	}

	inline static int32_t get_offset_of_dummyTarget_5() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___dummyTarget_5)); }
	inline Transform_t3600365921 * get_dummyTarget_5() const { return ___dummyTarget_5; }
	inline Transform_t3600365921 ** get_address_of_dummyTarget_5() { return &___dummyTarget_5; }
	inline void set_dummyTarget_5(Transform_t3600365921 * value)
	{
		___dummyTarget_5 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_5), value);
	}

	inline static int32_t get_offset_of_CameraTarget_6() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraTarget_6)); }
	inline Transform_t3600365921 * get_CameraTarget_6() const { return ___CameraTarget_6; }
	inline Transform_t3600365921 ** get_address_of_CameraTarget_6() { return &___CameraTarget_6; }
	inline void set_CameraTarget_6(Transform_t3600365921 * value)
	{
		___CameraTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_6), value);
	}

	inline static int32_t get_offset_of_FollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___FollowDistance_7)); }
	inline float get_FollowDistance_7() const { return ___FollowDistance_7; }
	inline float* get_address_of_FollowDistance_7() { return &___FollowDistance_7; }
	inline void set_FollowDistance_7(float value)
	{
		___FollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_8() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxFollowDistance_8)); }
	inline float get_MaxFollowDistance_8() const { return ___MaxFollowDistance_8; }
	inline float* get_address_of_MaxFollowDistance_8() { return &___MaxFollowDistance_8; }
	inline void set_MaxFollowDistance_8(float value)
	{
		___MaxFollowDistance_8 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_9() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinFollowDistance_9)); }
	inline float get_MinFollowDistance_9() const { return ___MinFollowDistance_9; }
	inline float* get_address_of_MinFollowDistance_9() { return &___MinFollowDistance_9; }
	inline void set_MinFollowDistance_9(float value)
	{
		___MinFollowDistance_9 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___ElevationAngle_10)); }
	inline float get_ElevationAngle_10() const { return ___ElevationAngle_10; }
	inline float* get_address_of_ElevationAngle_10() { return &___ElevationAngle_10; }
	inline void set_ElevationAngle_10(float value)
	{
		___ElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxElevationAngle_11)); }
	inline float get_MaxElevationAngle_11() const { return ___MaxElevationAngle_11; }
	inline float* get_address_of_MaxElevationAngle_11() { return &___MaxElevationAngle_11; }
	inline void set_MaxElevationAngle_11(float value)
	{
		___MaxElevationAngle_11 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_12() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinElevationAngle_12)); }
	inline float get_MinElevationAngle_12() const { return ___MinElevationAngle_12; }
	inline float* get_address_of_MinElevationAngle_12() { return &___MinElevationAngle_12; }
	inline void set_MinElevationAngle_12(float value)
	{
		___MinElevationAngle_12 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_13() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___OrbitalAngle_13)); }
	inline float get_OrbitalAngle_13() const { return ___OrbitalAngle_13; }
	inline float* get_address_of_OrbitalAngle_13() { return &___OrbitalAngle_13; }
	inline void set_OrbitalAngle_13(float value)
	{
		___OrbitalAngle_13 = value;
	}

	inline static int32_t get_offset_of_CameraMode_14() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraMode_14)); }
	inline int32_t get_CameraMode_14() const { return ___CameraMode_14; }
	inline int32_t* get_address_of_CameraMode_14() { return &___CameraMode_14; }
	inline void set_CameraMode_14(int32_t value)
	{
		___CameraMode_14 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothing_15)); }
	inline bool get_MovementSmoothing_15() const { return ___MovementSmoothing_15; }
	inline bool* get_address_of_MovementSmoothing_15() { return &___MovementSmoothing_15; }
	inline void set_MovementSmoothing_15(bool value)
	{
		___MovementSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_16() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothing_16)); }
	inline bool get_RotationSmoothing_16() const { return ___RotationSmoothing_16; }
	inline bool* get_address_of_RotationSmoothing_16() { return &___RotationSmoothing_16; }
	inline void set_RotationSmoothing_16(bool value)
	{
		___RotationSmoothing_16 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_17() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___previousSmoothing_17)); }
	inline bool get_previousSmoothing_17() const { return ___previousSmoothing_17; }
	inline bool* get_address_of_previousSmoothing_17() { return &___previousSmoothing_17; }
	inline void set_previousSmoothing_17(bool value)
	{
		___previousSmoothing_17 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_18() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothingValue_18)); }
	inline float get_MovementSmoothingValue_18() const { return ___MovementSmoothingValue_18; }
	inline float* get_address_of_MovementSmoothingValue_18() { return &___MovementSmoothingValue_18; }
	inline void set_MovementSmoothingValue_18(float value)
	{
		___MovementSmoothingValue_18 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_19() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothingValue_19)); }
	inline float get_RotationSmoothingValue_19() const { return ___RotationSmoothingValue_19; }
	inline float* get_address_of_RotationSmoothingValue_19() { return &___RotationSmoothingValue_19; }
	inline void set_RotationSmoothingValue_19(float value)
	{
		___RotationSmoothingValue_19 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_20() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MoveSensitivity_20)); }
	inline float get_MoveSensitivity_20() const { return ___MoveSensitivity_20; }
	inline float* get_address_of_MoveSensitivity_20() { return &___MoveSensitivity_20; }
	inline void set_MoveSensitivity_20(float value)
	{
		___MoveSensitivity_20 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_21() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___currentVelocity_21)); }
	inline Vector3_t3722313464  get_currentVelocity_21() const { return ___currentVelocity_21; }
	inline Vector3_t3722313464 * get_address_of_currentVelocity_21() { return &___currentVelocity_21; }
	inline void set_currentVelocity_21(Vector3_t3722313464  value)
	{
		___currentVelocity_21 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_22() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___desiredPosition_22)); }
	inline Vector3_t3722313464  get_desiredPosition_22() const { return ___desiredPosition_22; }
	inline Vector3_t3722313464 * get_address_of_desiredPosition_22() { return &___desiredPosition_22; }
	inline void set_desiredPosition_22(Vector3_t3722313464  value)
	{
		___desiredPosition_22 = value;
	}

	inline static int32_t get_offset_of_mouseX_23() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseX_23)); }
	inline float get_mouseX_23() const { return ___mouseX_23; }
	inline float* get_address_of_mouseX_23() { return &___mouseX_23; }
	inline void set_mouseX_23(float value)
	{
		___mouseX_23 = value;
	}

	inline static int32_t get_offset_of_mouseY_24() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseY_24)); }
	inline float get_mouseY_24() const { return ___mouseY_24; }
	inline float* get_address_of_mouseY_24() { return &___mouseY_24; }
	inline void set_mouseY_24(float value)
	{
		___mouseY_24 = value;
	}

	inline static int32_t get_offset_of_moveVector_25() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___moveVector_25)); }
	inline Vector3_t3722313464  get_moveVector_25() const { return ___moveVector_25; }
	inline Vector3_t3722313464 * get_address_of_moveVector_25() { return &___moveVector_25; }
	inline void set_moveVector_25(Vector3_t3722313464  value)
	{
		___moveVector_25 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_26() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseWheel_26)); }
	inline float get_mouseWheel_26() const { return ___mouseWheel_26; }
	inline float* get_address_of_mouseWheel_26() { return &___mouseWheel_26; }
	inline void set_mouseWheel_26(float value)
	{
		___mouseWheel_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T2264742161_H
#ifndef OBJECTSPIN_T341713598_H
#define OBJECTSPIN_T341713598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t341713598  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_4;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_5;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_t3600365921 * ___m_transform_6;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t3722313464  ___m_prevPOS_8;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t3722313464  ___m_initial_Rotation_9;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t3722313464  ___m_initial_Position_10;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t2600501292  ___m_lightColor_11;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_12;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_13;

public:
	inline static int32_t get_offset_of_SpinSpeed_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___SpinSpeed_4)); }
	inline float get_SpinSpeed_4() const { return ___SpinSpeed_4; }
	inline float* get_address_of_SpinSpeed_4() { return &___SpinSpeed_4; }
	inline void set_SpinSpeed_4(float value)
	{
		___SpinSpeed_4 = value;
	}

	inline static int32_t get_offset_of_RotationRange_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___RotationRange_5)); }
	inline int32_t get_RotationRange_5() const { return ___RotationRange_5; }
	inline int32_t* get_address_of_RotationRange_5() { return &___RotationRange_5; }
	inline void set_RotationRange_5(int32_t value)
	{
		___RotationRange_5 = value;
	}

	inline static int32_t get_offset_of_m_transform_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_transform_6)); }
	inline Transform_t3600365921 * get_m_transform_6() const { return ___m_transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_transform_6() { return &___m_transform_6; }
	inline void set_m_transform_6(Transform_t3600365921 * value)
	{
		___m_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_6), value);
	}

	inline static int32_t get_offset_of_m_time_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_time_7)); }
	inline float get_m_time_7() const { return ___m_time_7; }
	inline float* get_address_of_m_time_7() { return &___m_time_7; }
	inline void set_m_time_7(float value)
	{
		___m_time_7 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_prevPOS_8)); }
	inline Vector3_t3722313464  get_m_prevPOS_8() const { return ___m_prevPOS_8; }
	inline Vector3_t3722313464 * get_address_of_m_prevPOS_8() { return &___m_prevPOS_8; }
	inline void set_m_prevPOS_8(Vector3_t3722313464  value)
	{
		___m_prevPOS_8 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Rotation_9)); }
	inline Vector3_t3722313464  get_m_initial_Rotation_9() const { return ___m_initial_Rotation_9; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Rotation_9() { return &___m_initial_Rotation_9; }
	inline void set_m_initial_Rotation_9(Vector3_t3722313464  value)
	{
		___m_initial_Rotation_9 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Position_10)); }
	inline Vector3_t3722313464  get_m_initial_Position_10() const { return ___m_initial_Position_10; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Position_10() { return &___m_initial_Position_10; }
	inline void set_m_initial_Position_10(Vector3_t3722313464  value)
	{
		___m_initial_Position_10 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_lightColor_11)); }
	inline Color32_t2600501292  get_m_lightColor_11() const { return ___m_lightColor_11; }
	inline Color32_t2600501292 * get_address_of_m_lightColor_11() { return &___m_lightColor_11; }
	inline void set_m_lightColor_11(Color32_t2600501292  value)
	{
		___m_lightColor_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___frames_12)); }
	inline int32_t get_frames_12() const { return ___frames_12; }
	inline int32_t* get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(int32_t value)
	{
		___frames_12 = value;
	}

	inline static int32_t get_offset_of_Motion_13() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___Motion_13)); }
	inline int32_t get_Motion_13() const { return ___Motion_13; }
	inline int32_t* get_address_of_Motion_13() { return &___Motion_13; }
	inline void set_Motion_13(int32_t value)
	{
		___Motion_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T341713598_H
#ifndef SHADERPROPANIMATOR_T3617420994_H
#define SHADERPROPANIMATOR_T3617420994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_t3617420994  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t2627027031 * ___m_Renderer_4;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t340375123 * ___m_Material_5;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t3046754366 * ___GlowCurve_6;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_7;

public:
	inline static int32_t get_offset_of_m_Renderer_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Renderer_4)); }
	inline Renderer_t2627027031 * get_m_Renderer_4() const { return ___m_Renderer_4; }
	inline Renderer_t2627027031 ** get_address_of_m_Renderer_4() { return &___m_Renderer_4; }
	inline void set_m_Renderer_4(Renderer_t2627027031 * value)
	{
		___m_Renderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_4), value);
	}

	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Material_5)); }
	inline Material_t340375123 * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_t340375123 ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_t340375123 * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_5), value);
	}

	inline static int32_t get_offset_of_GlowCurve_6() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___GlowCurve_6)); }
	inline AnimationCurve_t3046754366 * get_GlowCurve_6() const { return ___GlowCurve_6; }
	inline AnimationCurve_t3046754366 ** get_address_of_GlowCurve_6() { return &___GlowCurve_6; }
	inline void set_GlowCurve_6(AnimationCurve_t3046754366 * value)
	{
		___GlowCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_6), value);
	}

	inline static int32_t get_offset_of_m_frame_7() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_frame_7)); }
	inline float get_m_frame_7() const { return ___m_frame_7; }
	inline float* get_address_of_m_frame_7() { return &___m_frame_7; }
	inline void set_m_frame_7(float value)
	{
		___m_frame_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_T3617420994_H
#ifndef SIMPLESCRIPT_T3279312205_H
#define SIMPLESCRIPT_T3279312205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_t3279312205  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_4;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_6;

public:
	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_textMeshPro_4)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_frame_6() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_frame_6)); }
	inline float get_m_frame_6() const { return ___m_frame_6; }
	inline float* get_address_of_m_frame_6() { return &___m_frame_6; }
	inline void set_m_frame_6(float value)
	{
		___m_frame_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_T3279312205_H
#ifndef SKEWTEXTEXAMPLE_T3460249701_H
#define SKEWTEXTEXAMPLE_T3460249701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample
struct  SkewTextExample_t3460249701  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.SkewTextExample::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_4;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::VertexCurve
	AnimationCurve_t3046754366 * ___VertexCurve_5;
	// System.Single TMPro.Examples.SkewTextExample::CurveScale
	float ___CurveScale_6;
	// System.Single TMPro.Examples.SkewTextExample::ShearAmount
	float ___ShearAmount_7;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___m_TextComponent_4)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}

	inline static int32_t get_offset_of_VertexCurve_5() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___VertexCurve_5)); }
	inline AnimationCurve_t3046754366 * get_VertexCurve_5() const { return ___VertexCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_VertexCurve_5() { return &___VertexCurve_5; }
	inline void set_VertexCurve_5(AnimationCurve_t3046754366 * value)
	{
		___VertexCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_5), value);
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_ShearAmount_7() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___ShearAmount_7)); }
	inline float get_ShearAmount_7() const { return ___ShearAmount_7; }
	inline float* get_address_of_ShearAmount_7() { return &___ShearAmount_7; }
	inline void set_ShearAmount_7(float value)
	{
		___ShearAmount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEWTEXTEXAMPLE_T3460249701_H
#ifndef TMP_EXAMPLESCRIPT_01_T3051742005_H
#define TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01
struct  TMP_ExampleScript_01_t3051742005  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.Examples.TMP_ExampleScript_01/objectType TMPro.Examples.TMP_ExampleScript_01::ObjectType
	int32_t ___ObjectType_4;
	// System.Boolean TMPro.Examples.TMP_ExampleScript_01::isStatic
	bool ___isStatic_5;
	// TMPro.TMP_Text TMPro.Examples.TMP_ExampleScript_01::m_text
	TMP_Text_t2599618874 * ___m_text_6;
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01::count
	int32_t ___count_8;

public:
	inline static int32_t get_offset_of_ObjectType_4() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___ObjectType_4)); }
	inline int32_t get_ObjectType_4() const { return ___ObjectType_4; }
	inline int32_t* get_address_of_ObjectType_4() { return &___ObjectType_4; }
	inline void set_ObjectType_4(int32_t value)
	{
		___ObjectType_4 = value;
	}

	inline static int32_t get_offset_of_isStatic_5() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___isStatic_5)); }
	inline bool get_isStatic_5() const { return ___isStatic_5; }
	inline bool* get_address_of_isStatic_5() { return &___isStatic_5; }
	inline void set_isStatic_5(bool value)
	{
		___isStatic_5 = value;
	}

	inline static int32_t get_offset_of_m_text_6() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___m_text_6)); }
	inline TMP_Text_t2599618874 * get_m_text_6() const { return ___m_text_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_text_6() { return &___m_text_6; }
	inline void set_m_text_6(TMP_Text_t2599618874 * value)
	{
		___m_text_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_6), value);
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifndef TMP_FRAMERATECOUNTER_T314972976_H
#define TMP_FRAMERATECOUNTER_T314972976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter
struct  TMP_FrameRateCounter_t314972976  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_FrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_FrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_FrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshPro TMPro.Examples.TMP_FrameRateCounter::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_10;
	// UnityEngine.Transform TMPro.Examples.TMP_FrameRateCounter::m_frameCounter_transform
	Transform_t3600365921 * ___m_frameCounter_transform_11;
	// UnityEngine.Camera TMPro.Examples.TMP_FrameRateCounter::m_camera
	Camera_t4157153871 * ___m_camera_12;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_13;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_8), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_TextMeshPro_10)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_10), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_frameCounter_transform_11)); }
	inline Transform_t3600365921 * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline Transform_t3600365921 ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(Transform_t3600365921 * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_11), value);
	}

	inline static int32_t get_offset_of_m_camera_12() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_camera_12)); }
	inline Camera_t4157153871 * get_m_camera_12() const { return ___m_camera_12; }
	inline Camera_t4157153871 ** get_address_of_m_camera_12() { return &___m_camera_12; }
	inline void set_m_camera_12(Camera_t4157153871 * value)
	{
		___m_camera_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_12), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_13() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___last_AnchorPosition_13)); }
	inline int32_t get_last_AnchorPosition_13() const { return ___last_AnchorPosition_13; }
	inline int32_t* get_address_of_last_AnchorPosition_13() { return &___last_AnchorPosition_13; }
	inline void set_last_AnchorPosition_13(int32_t value)
	{
		___last_AnchorPosition_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FRAMERATECOUNTER_T314972976_H
#ifndef TELETYPE_T2409835159_H
#define TELETYPE_T2409835159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType
struct  TeleType_t2409835159  : public MonoBehaviour_t3962482529
{
public:
	// System.String TMPro.Examples.TeleType::label01
	String_t* ___label01_4;
	// System.String TMPro.Examples.TeleType::label02
	String_t* ___label02_5;
	// TMPro.TMP_Text TMPro.Examples.TeleType::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_6;

public:
	inline static int32_t get_offset_of_label01_4() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label01_4)); }
	inline String_t* get_label01_4() const { return ___label01_4; }
	inline String_t** get_address_of_label01_4() { return &___label01_4; }
	inline void set_label01_4(String_t* value)
	{
		___label01_4 = value;
		Il2CppCodeGenWriteBarrier((&___label01_4), value);
	}

	inline static int32_t get_offset_of_label02_5() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label02_5)); }
	inline String_t* get_label02_5() const { return ___label02_5; }
	inline String_t** get_address_of_label02_5() { return &___label02_5; }
	inline void set_label02_5(String_t* value)
	{
		___label02_5 = value;
		Il2CppCodeGenWriteBarrier((&___label02_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___m_textMeshPro_6)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETYPE_T2409835159_H
#ifndef TEXTCONSOLESIMULATOR_T3766250034_H
#define TEXTCONSOLESIMULATOR_T3766250034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator
struct  TextConsoleSimulator_t3766250034  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_4;
	// System.Boolean TMPro.Examples.TextConsoleSimulator::hasTextChanged
	bool ___hasTextChanged_5;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___m_TextComponent_4)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_5() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___hasTextChanged_5)); }
	inline bool get_hasTextChanged_5() const { return ___hasTextChanged_5; }
	inline bool* get_address_of_hasTextChanged_5() { return &___hasTextChanged_5; }
	inline void set_hasTextChanged_5(bool value)
	{
		___hasTextChanged_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONSOLESIMULATOR_T3766250034_H
#ifndef TEXTMESHPROFLOATINGTEXT_T845872552_H
#define TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText
struct  TextMeshProFloatingText_t845872552  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Font TMPro.Examples.TextMeshProFloatingText::TheFont
	Font_t1956802104 * ___TheFont_4;
	// UnityEngine.GameObject TMPro.Examples.TextMeshProFloatingText::m_floatingText
	GameObject_t1113636619 * ___m_floatingText_5;
	// TMPro.TextMeshPro TMPro.Examples.TextMeshProFloatingText::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_6;
	// UnityEngine.TextMesh TMPro.Examples.TextMeshProFloatingText::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_7;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_transform
	Transform_t3600365921 * ___m_transform_8;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_floatingText_Transform
	Transform_t3600365921 * ___m_floatingText_Transform_9;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_cameraTransform
	Transform_t3600365921 * ___m_cameraTransform_10;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText::lastPOS
	Vector3_t3722313464  ___lastPOS_11;
	// UnityEngine.Quaternion TMPro.Examples.TextMeshProFloatingText::lastRotation
	Quaternion_t2301928331  ___lastRotation_12;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText::SpawnType
	int32_t ___SpawnType_13;

public:
	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}

	inline static int32_t get_offset_of_m_floatingText_5() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_5)); }
	inline GameObject_t1113636619 * get_m_floatingText_5() const { return ___m_floatingText_5; }
	inline GameObject_t1113636619 ** get_address_of_m_floatingText_5() { return &___m_floatingText_5; }
	inline void set_m_floatingText_5(GameObject_t1113636619 * value)
	{
		___m_floatingText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMeshPro_6)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMesh_7)); }
	inline TextMesh_t1536577757 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1536577757 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_transform_8() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_transform_8)); }
	inline Transform_t3600365921 * get_m_transform_8() const { return ___m_transform_8; }
	inline Transform_t3600365921 ** get_address_of_m_transform_8() { return &___m_transform_8; }
	inline void set_m_transform_8(Transform_t3600365921 * value)
	{
		___m_transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_8), value);
	}

	inline static int32_t get_offset_of_m_floatingText_Transform_9() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_Transform_9)); }
	inline Transform_t3600365921 * get_m_floatingText_Transform_9() const { return ___m_floatingText_Transform_9; }
	inline Transform_t3600365921 ** get_address_of_m_floatingText_Transform_9() { return &___m_floatingText_Transform_9; }
	inline void set_m_floatingText_Transform_9(Transform_t3600365921 * value)
	{
		___m_floatingText_Transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_Transform_9), value);
	}

	inline static int32_t get_offset_of_m_cameraTransform_10() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_cameraTransform_10)); }
	inline Transform_t3600365921 * get_m_cameraTransform_10() const { return ___m_cameraTransform_10; }
	inline Transform_t3600365921 ** get_address_of_m_cameraTransform_10() { return &___m_cameraTransform_10; }
	inline void set_m_cameraTransform_10(Transform_t3600365921 * value)
	{
		___m_cameraTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_cameraTransform_10), value);
	}

	inline static int32_t get_offset_of_lastPOS_11() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastPOS_11)); }
	inline Vector3_t3722313464  get_lastPOS_11() const { return ___lastPOS_11; }
	inline Vector3_t3722313464 * get_address_of_lastPOS_11() { return &___lastPOS_11; }
	inline void set_lastPOS_11(Vector3_t3722313464  value)
	{
		___lastPOS_11 = value;
	}

	inline static int32_t get_offset_of_lastRotation_12() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastRotation_12)); }
	inline Quaternion_t2301928331  get_lastRotation_12() const { return ___lastRotation_12; }
	inline Quaternion_t2301928331 * get_address_of_lastRotation_12() { return &___lastRotation_12; }
	inline void set_lastRotation_12(Quaternion_t2301928331  value)
	{
		___lastRotation_12 = value;
	}

	inline static int32_t get_offset_of_SpawnType_13() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___SpawnType_13)); }
	inline int32_t get_SpawnType_13() const { return ___SpawnType_13; }
	inline int32_t* get_address_of_SpawnType_13() { return &___SpawnType_13; }
	inline void set_SpawnType_13(int32_t value)
	{
		___SpawnType_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifndef TEXTMESHSPAWNER_T177691618_H
#define TEXTMESHSPAWNER_T177691618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshSpawner
struct  TextMeshSpawner_t177691618  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_t1956802104 * ___TheFont_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_7;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___TheFont_6)); }
	inline Font_t1956802104 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_t1956802104 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_t1956802104 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_6), value);
	}

	inline static int32_t get_offset_of_floatingText_Script_7() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___floatingText_Script_7)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_7() const { return ___floatingText_Script_7; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_7() { return &___floatingText_Script_7; }
	inline void set_floatingText_Script_7(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_7 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHSPAWNER_T177691618_H
#ifndef UIBEHAVIOUR_T1116137811_H
#define UIBEHAVIOUR_T1116137811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBehaviour
struct  UIBehaviour_t1116137811  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UIBehaviour::mButtons
	List_1_t2585711361 * ___mButtons_4;

public:
	inline static int32_t get_offset_of_mButtons_4() { return static_cast<int32_t>(offsetof(UIBehaviour_t1116137811, ___mButtons_4)); }
	inline List_1_t2585711361 * get_mButtons_4() const { return ___mButtons_4; }
	inline List_1_t2585711361 ** get_address_of_mButtons_4() { return &___mButtons_4; }
	inline void set_mButtons_4(List_1_t2585711361 * value)
	{
		___mButtons_4 = value;
		Il2CppCodeGenWriteBarrier((&___mButtons_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T1116137811_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_4;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_5;

public:
	inline static int32_t get_offset_of_m_Trigger_4() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_4)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_4() const { return ___m_Trigger_4; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_4() { return &___m_Trigger_4; }
	inline void set_m_Trigger_4(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_4), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_5() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_5)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_5() const { return ___m_EventPayload_5; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_5() { return &___m_EventPayload_5; }
	inline void set_m_EventPayload_5(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_5;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_6;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_7;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_8;

public:
	inline static int32_t get_offset_of_m_EventName_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_4)); }
	inline String_t* get_m_EventName_4() const { return ___m_EventName_4; }
	inline String_t** get_address_of_m_EventName_4() { return &___m_EventName_4; }
	inline void set_m_EventName_4(String_t* value)
	{
		___m_EventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_4), value);
	}

	inline static int32_t get_offset_of_m_Dict_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_5)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_5() const { return ___m_Dict_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_5() { return &___m_Dict_5; }
	inline void set_m_Dict_5(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_5), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_6)); }
	inline int32_t get_m_PrevDictHash_6() const { return ___m_PrevDictHash_6; }
	inline int32_t* get_address_of_m_PrevDictHash_6() { return &___m_PrevDictHash_6; }
	inline void set_m_PrevDictHash_6(int32_t value)
	{
		___m_PrevDictHash_6 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_7() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_7)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_7() const { return ___m_TrackableProperty_7; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_7() { return &___m_TrackableProperty_7; }
	inline void set_m_TrackableProperty_7(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_7), value);
	}

	inline static int32_t get_offset_of_m_Trigger_8() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_8)); }
	inline int32_t get_m_Trigger_8() const { return ___m_Trigger_8; }
	inline int32_t* get_address_of_m_Trigger_8() { return &___m_Trigger_8; }
	inline void set_m_Trigger_8(int32_t value)
	{
		___m_Trigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef FLOORLOGIC_T3814374729_H
#define FLOORLOGIC_T3814374729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.floorLogic.floorLogic
struct  floorLogic_t3814374729  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.String> assets.floorLogic.floorLogic::constantPoints
	List_1_t3319525431 * ___constantPoints_8;

public:
	inline static int32_t get_offset_of_constantPoints_8() { return static_cast<int32_t>(offsetof(floorLogic_t3814374729, ___constantPoints_8)); }
	inline List_1_t3319525431 * get_constantPoints_8() const { return ___constantPoints_8; }
	inline List_1_t3319525431 ** get_address_of_constantPoints_8() { return &___constantPoints_8; }
	inline void set_constantPoints_8(List_1_t3319525431 * value)
	{
		___constantPoints_8 = value;
		Il2CppCodeGenWriteBarrier((&___constantPoints_8), value);
	}
};

struct floorLogic_t3814374729_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> assets.floorLogic.floorLogic::allObjects
	List_1_t2585711361 * ___allObjects_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> assets.floorLogic.floorLogic::hiddenObjects
	List_1_t2585711361 * ___hiddenObjects_6;
	// System.String assets.floorLogic.floorLogic::currentBuilding
	String_t* ___currentBuilding_7;

public:
	inline static int32_t get_offset_of_allObjects_5() { return static_cast<int32_t>(offsetof(floorLogic_t3814374729_StaticFields, ___allObjects_5)); }
	inline List_1_t2585711361 * get_allObjects_5() const { return ___allObjects_5; }
	inline List_1_t2585711361 ** get_address_of_allObjects_5() { return &___allObjects_5; }
	inline void set_allObjects_5(List_1_t2585711361 * value)
	{
		___allObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___allObjects_5), value);
	}

	inline static int32_t get_offset_of_hiddenObjects_6() { return static_cast<int32_t>(offsetof(floorLogic_t3814374729_StaticFields, ___hiddenObjects_6)); }
	inline List_1_t2585711361 * get_hiddenObjects_6() const { return ___hiddenObjects_6; }
	inline List_1_t2585711361 ** get_address_of_hiddenObjects_6() { return &___hiddenObjects_6; }
	inline void set_hiddenObjects_6(List_1_t2585711361 * value)
	{
		___hiddenObjects_6 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenObjects_6), value);
	}

	inline static int32_t get_offset_of_currentBuilding_7() { return static_cast<int32_t>(offsetof(floorLogic_t3814374729_StaticFields, ___currentBuilding_7)); }
	inline String_t* get_currentBuilding_7() const { return ___currentBuilding_7; }
	inline String_t** get_address_of_currentBuilding_7() { return &___currentBuilding_7; }
	inline void set_currentBuilding_7(String_t* value)
	{
		___currentBuilding_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentBuilding_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOORLOGIC_T3814374729_H
#ifndef PATHLOGIC_T3937024697_H
#define PATHLOGIC_T3937024697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.pathLogic.pathLogic
struct  pathLogic_t3937024697  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material assets.pathLogic.pathLogic::lineMat
	Material_t340375123 * ___lineMat_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> assets.pathLogic.pathLogic::points
	List_1_t2585711361 * ___points_5;
	// UnityEngine.Networking.UnityWebRequest assets.pathLogic.pathLogic::request
	UnityWebRequest_t463507806 * ___request_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> assets.pathLogic.pathLogic::path
	List_1_t2585711361 * ___path_7;
	// UnityEngine.UI.Dropdown assets.pathLogic.pathLogic::dropdownList
	Dropdown_t2274391225 * ___dropdownList_8;
	// UnityEngine.UI.Dropdown assets.pathLogic.pathLogic::dropdownListStart
	Dropdown_t2274391225 * ___dropdownListStart_9;
	// System.String assets.pathLogic.pathLogic::dest
	String_t* ___dest_11;
	// System.String assets.pathLogic.pathLogic::url
	String_t* ___url_12;
	// System.Int32 assets.pathLogic.pathLogic::startingRange
	int32_t ___startingRange_13;
	// System.Int32 assets.pathLogic.pathLogic::finishingRange
	int32_t ___finishingRange_14;

public:
	inline static int32_t get_offset_of_lineMat_4() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___lineMat_4)); }
	inline Material_t340375123 * get_lineMat_4() const { return ___lineMat_4; }
	inline Material_t340375123 ** get_address_of_lineMat_4() { return &___lineMat_4; }
	inline void set_lineMat_4(Material_t340375123 * value)
	{
		___lineMat_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineMat_4), value);
	}

	inline static int32_t get_offset_of_points_5() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___points_5)); }
	inline List_1_t2585711361 * get_points_5() const { return ___points_5; }
	inline List_1_t2585711361 ** get_address_of_points_5() { return &___points_5; }
	inline void set_points_5(List_1_t2585711361 * value)
	{
		___points_5 = value;
		Il2CppCodeGenWriteBarrier((&___points_5), value);
	}

	inline static int32_t get_offset_of_request_6() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___request_6)); }
	inline UnityWebRequest_t463507806 * get_request_6() const { return ___request_6; }
	inline UnityWebRequest_t463507806 ** get_address_of_request_6() { return &___request_6; }
	inline void set_request_6(UnityWebRequest_t463507806 * value)
	{
		___request_6 = value;
		Il2CppCodeGenWriteBarrier((&___request_6), value);
	}

	inline static int32_t get_offset_of_path_7() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___path_7)); }
	inline List_1_t2585711361 * get_path_7() const { return ___path_7; }
	inline List_1_t2585711361 ** get_address_of_path_7() { return &___path_7; }
	inline void set_path_7(List_1_t2585711361 * value)
	{
		___path_7 = value;
		Il2CppCodeGenWriteBarrier((&___path_7), value);
	}

	inline static int32_t get_offset_of_dropdownList_8() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___dropdownList_8)); }
	inline Dropdown_t2274391225 * get_dropdownList_8() const { return ___dropdownList_8; }
	inline Dropdown_t2274391225 ** get_address_of_dropdownList_8() { return &___dropdownList_8; }
	inline void set_dropdownList_8(Dropdown_t2274391225 * value)
	{
		___dropdownList_8 = value;
		Il2CppCodeGenWriteBarrier((&___dropdownList_8), value);
	}

	inline static int32_t get_offset_of_dropdownListStart_9() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___dropdownListStart_9)); }
	inline Dropdown_t2274391225 * get_dropdownListStart_9() const { return ___dropdownListStart_9; }
	inline Dropdown_t2274391225 ** get_address_of_dropdownListStart_9() { return &___dropdownListStart_9; }
	inline void set_dropdownListStart_9(Dropdown_t2274391225 * value)
	{
		___dropdownListStart_9 = value;
		Il2CppCodeGenWriteBarrier((&___dropdownListStart_9), value);
	}

	inline static int32_t get_offset_of_dest_11() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___dest_11)); }
	inline String_t* get_dest_11() const { return ___dest_11; }
	inline String_t** get_address_of_dest_11() { return &___dest_11; }
	inline void set_dest_11(String_t* value)
	{
		___dest_11 = value;
		Il2CppCodeGenWriteBarrier((&___dest_11), value);
	}

	inline static int32_t get_offset_of_url_12() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___url_12)); }
	inline String_t* get_url_12() const { return ___url_12; }
	inline String_t** get_address_of_url_12() { return &___url_12; }
	inline void set_url_12(String_t* value)
	{
		___url_12 = value;
		Il2CppCodeGenWriteBarrier((&___url_12), value);
	}

	inline static int32_t get_offset_of_startingRange_13() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___startingRange_13)); }
	inline int32_t get_startingRange_13() const { return ___startingRange_13; }
	inline int32_t* get_address_of_startingRange_13() { return &___startingRange_13; }
	inline void set_startingRange_13(int32_t value)
	{
		___startingRange_13 = value;
	}

	inline static int32_t get_offset_of_finishingRange_14() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697, ___finishingRange_14)); }
	inline int32_t get_finishingRange_14() const { return ___finishingRange_14; }
	inline int32_t* get_address_of_finishingRange_14() { return &___finishingRange_14; }
	inline void set_finishingRange_14(int32_t value)
	{
		___finishingRange_14 = value;
	}
};

struct pathLogic_t3937024697_StaticFields
{
public:
	// System.String assets.pathLogic.pathLogic::start
	String_t* ___start_10;
	// System.Predicate`1<UnityEngine.GameObject> assets.pathLogic.pathLogic::<>f__am$cache0
	Predicate_1_t1938930743 * ___U3CU3Ef__amU24cache0_15;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> assets.pathLogic.pathLogic::<>f__am$cache1
	Func_2_t4243939292 * ___U3CU3Ef__amU24cache1_16;

public:
	inline static int32_t get_offset_of_start_10() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697_StaticFields, ___start_10)); }
	inline String_t* get_start_10() const { return ___start_10; }
	inline String_t** get_address_of_start_10() { return &___start_10; }
	inline void set_start_10(String_t* value)
	{
		___start_10 = value;
		Il2CppCodeGenWriteBarrier((&___start_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Predicate_1_t1938930743 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Predicate_1_t1938930743 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Predicate_1_t1938930743 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_16() { return static_cast<int32_t>(offsetof(pathLogic_t3937024697_StaticFields, ___U3CU3Ef__amU24cache1_16)); }
	inline Func_2_t4243939292 * get_U3CU3Ef__amU24cache1_16() const { return ___U3CU3Ef__amU24cache1_16; }
	inline Func_2_t4243939292 ** get_address_of_U3CU3Ef__amU24cache1_16() { return &___U3CU3Ef__amU24cache1_16; }
	inline void set_U3CU3Ef__amU24cache1_16(Func_2_t4243939292 * value)
	{
		___U3CU3Ef__amU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHLOGIC_T3937024697_H
#ifndef POINTSLOGIC_T669785447_H
#define POINTSLOGIC_T669785447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.pointsLogic.pointsLogic
struct  pointsLogic_t669785447  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Dropdown assets.pointsLogic.pointsLogic::mDropdown
	Dropdown_t2274391225 * ___mDropdown_4;
	// UnityEngine.UI.Dropdown assets.pointsLogic.pointsLogic::mDropDownStart
	Dropdown_t2274391225 * ___mDropDownStart_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> assets.pointsLogic.pointsLogic::points
	List_1_t2585711361 * ___points_6;
	// System.Collections.Generic.List`1<System.String> assets.pointsLogic.pointsLogic::pointNames
	List_1_t3319525431 * ___pointNames_7;

public:
	inline static int32_t get_offset_of_mDropdown_4() { return static_cast<int32_t>(offsetof(pointsLogic_t669785447, ___mDropdown_4)); }
	inline Dropdown_t2274391225 * get_mDropdown_4() const { return ___mDropdown_4; }
	inline Dropdown_t2274391225 ** get_address_of_mDropdown_4() { return &___mDropdown_4; }
	inline void set_mDropdown_4(Dropdown_t2274391225 * value)
	{
		___mDropdown_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDropdown_4), value);
	}

	inline static int32_t get_offset_of_mDropDownStart_5() { return static_cast<int32_t>(offsetof(pointsLogic_t669785447, ___mDropDownStart_5)); }
	inline Dropdown_t2274391225 * get_mDropDownStart_5() const { return ___mDropDownStart_5; }
	inline Dropdown_t2274391225 ** get_address_of_mDropDownStart_5() { return &___mDropDownStart_5; }
	inline void set_mDropDownStart_5(Dropdown_t2274391225 * value)
	{
		___mDropDownStart_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDropDownStart_5), value);
	}

	inline static int32_t get_offset_of_points_6() { return static_cast<int32_t>(offsetof(pointsLogic_t669785447, ___points_6)); }
	inline List_1_t2585711361 * get_points_6() const { return ___points_6; }
	inline List_1_t2585711361 ** get_address_of_points_6() { return &___points_6; }
	inline void set_points_6(List_1_t2585711361 * value)
	{
		___points_6 = value;
		Il2CppCodeGenWriteBarrier((&___points_6), value);
	}

	inline static int32_t get_offset_of_pointNames_7() { return static_cast<int32_t>(offsetof(pointsLogic_t669785447, ___pointNames_7)); }
	inline List_1_t3319525431 * get_pointNames_7() const { return ___pointNames_7; }
	inline List_1_t3319525431 ** get_address_of_pointNames_7() { return &___pointNames_7; }
	inline void set_pointNames_7(List_1_t3319525431 * value)
	{
		___pointNames_7 = value;
		Il2CppCodeGenWriteBarrier((&___pointNames_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTSLOGIC_T669785447_H
#ifndef PINCHZOOM_T4152403964_H
#define PINCHZOOM_T4152403964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.scripts.PinchZoom.PinchZoom
struct  PinchZoom_t4152403964  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 assets.scripts.PinchZoom.PinchZoom::firstpoint
	Vector3_t3722313464  ___firstpoint_4;
	// UnityEngine.Vector3 assets.scripts.PinchZoom.PinchZoom::secondpoint
	Vector3_t3722313464  ___secondpoint_5;
	// UnityEngine.Vector2 assets.scripts.PinchZoom.PinchZoom::worldStartPoint
	Vector2_t2156229523  ___worldStartPoint_6;
	// System.Single assets.scripts.PinchZoom.PinchZoom::PanSpeed
	float ___PanSpeed_7;
	// System.Single assets.scripts.PinchZoom.PinchZoom::ZoomSpeedTouch
	float ___ZoomSpeedTouch_8;
	// System.Single assets.scripts.PinchZoom.PinchZoom::ZoomSpeedMouse
	float ___ZoomSpeedMouse_9;
	// UnityEngine.Camera assets.scripts.PinchZoom.PinchZoom::cam
	Camera_t4157153871 * ___cam_13;
	// UnityEngine.Vector3 assets.scripts.PinchZoom.PinchZoom::lastPanPosition
	Vector3_t3722313464  ___lastPanPosition_14;
	// System.Int32 assets.scripts.PinchZoom.PinchZoom::panFingerId
	int32_t ___panFingerId_15;
	// System.Boolean assets.scripts.PinchZoom.PinchZoom::wasZoomingLastFrame
	bool ___wasZoomingLastFrame_16;
	// UnityEngine.Vector2[] assets.scripts.PinchZoom.PinchZoom::lastZoomPositions
	Vector2U5BU5D_t1457185986* ___lastZoomPositions_17;

public:
	inline static int32_t get_offset_of_firstpoint_4() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___firstpoint_4)); }
	inline Vector3_t3722313464  get_firstpoint_4() const { return ___firstpoint_4; }
	inline Vector3_t3722313464 * get_address_of_firstpoint_4() { return &___firstpoint_4; }
	inline void set_firstpoint_4(Vector3_t3722313464  value)
	{
		___firstpoint_4 = value;
	}

	inline static int32_t get_offset_of_secondpoint_5() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___secondpoint_5)); }
	inline Vector3_t3722313464  get_secondpoint_5() const { return ___secondpoint_5; }
	inline Vector3_t3722313464 * get_address_of_secondpoint_5() { return &___secondpoint_5; }
	inline void set_secondpoint_5(Vector3_t3722313464  value)
	{
		___secondpoint_5 = value;
	}

	inline static int32_t get_offset_of_worldStartPoint_6() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___worldStartPoint_6)); }
	inline Vector2_t2156229523  get_worldStartPoint_6() const { return ___worldStartPoint_6; }
	inline Vector2_t2156229523 * get_address_of_worldStartPoint_6() { return &___worldStartPoint_6; }
	inline void set_worldStartPoint_6(Vector2_t2156229523  value)
	{
		___worldStartPoint_6 = value;
	}

	inline static int32_t get_offset_of_PanSpeed_7() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___PanSpeed_7)); }
	inline float get_PanSpeed_7() const { return ___PanSpeed_7; }
	inline float* get_address_of_PanSpeed_7() { return &___PanSpeed_7; }
	inline void set_PanSpeed_7(float value)
	{
		___PanSpeed_7 = value;
	}

	inline static int32_t get_offset_of_ZoomSpeedTouch_8() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___ZoomSpeedTouch_8)); }
	inline float get_ZoomSpeedTouch_8() const { return ___ZoomSpeedTouch_8; }
	inline float* get_address_of_ZoomSpeedTouch_8() { return &___ZoomSpeedTouch_8; }
	inline void set_ZoomSpeedTouch_8(float value)
	{
		___ZoomSpeedTouch_8 = value;
	}

	inline static int32_t get_offset_of_ZoomSpeedMouse_9() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___ZoomSpeedMouse_9)); }
	inline float get_ZoomSpeedMouse_9() const { return ___ZoomSpeedMouse_9; }
	inline float* get_address_of_ZoomSpeedMouse_9() { return &___ZoomSpeedMouse_9; }
	inline void set_ZoomSpeedMouse_9(float value)
	{
		___ZoomSpeedMouse_9 = value;
	}

	inline static int32_t get_offset_of_cam_13() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___cam_13)); }
	inline Camera_t4157153871 * get_cam_13() const { return ___cam_13; }
	inline Camera_t4157153871 ** get_address_of_cam_13() { return &___cam_13; }
	inline void set_cam_13(Camera_t4157153871 * value)
	{
		___cam_13 = value;
		Il2CppCodeGenWriteBarrier((&___cam_13), value);
	}

	inline static int32_t get_offset_of_lastPanPosition_14() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___lastPanPosition_14)); }
	inline Vector3_t3722313464  get_lastPanPosition_14() const { return ___lastPanPosition_14; }
	inline Vector3_t3722313464 * get_address_of_lastPanPosition_14() { return &___lastPanPosition_14; }
	inline void set_lastPanPosition_14(Vector3_t3722313464  value)
	{
		___lastPanPosition_14 = value;
	}

	inline static int32_t get_offset_of_panFingerId_15() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___panFingerId_15)); }
	inline int32_t get_panFingerId_15() const { return ___panFingerId_15; }
	inline int32_t* get_address_of_panFingerId_15() { return &___panFingerId_15; }
	inline void set_panFingerId_15(int32_t value)
	{
		___panFingerId_15 = value;
	}

	inline static int32_t get_offset_of_wasZoomingLastFrame_16() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___wasZoomingLastFrame_16)); }
	inline bool get_wasZoomingLastFrame_16() const { return ___wasZoomingLastFrame_16; }
	inline bool* get_address_of_wasZoomingLastFrame_16() { return &___wasZoomingLastFrame_16; }
	inline void set_wasZoomingLastFrame_16(bool value)
	{
		___wasZoomingLastFrame_16 = value;
	}

	inline static int32_t get_offset_of_lastZoomPositions_17() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964, ___lastZoomPositions_17)); }
	inline Vector2U5BU5D_t1457185986* get_lastZoomPositions_17() const { return ___lastZoomPositions_17; }
	inline Vector2U5BU5D_t1457185986** get_address_of_lastZoomPositions_17() { return &___lastZoomPositions_17; }
	inline void set_lastZoomPositions_17(Vector2U5BU5D_t1457185986* value)
	{
		___lastZoomPositions_17 = value;
		Il2CppCodeGenWriteBarrier((&___lastZoomPositions_17), value);
	}
};

struct PinchZoom_t4152403964_StaticFields
{
public:
	// System.Single[] assets.scripts.PinchZoom.PinchZoom::BoundsX
	SingleU5BU5D_t1444911251* ___BoundsX_10;
	// System.Single[] assets.scripts.PinchZoom.PinchZoom::BoundsZ
	SingleU5BU5D_t1444911251* ___BoundsZ_11;
	// System.Single[] assets.scripts.PinchZoom.PinchZoom::ZoomBounds
	SingleU5BU5D_t1444911251* ___ZoomBounds_12;

public:
	inline static int32_t get_offset_of_BoundsX_10() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964_StaticFields, ___BoundsX_10)); }
	inline SingleU5BU5D_t1444911251* get_BoundsX_10() const { return ___BoundsX_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_BoundsX_10() { return &___BoundsX_10; }
	inline void set_BoundsX_10(SingleU5BU5D_t1444911251* value)
	{
		___BoundsX_10 = value;
		Il2CppCodeGenWriteBarrier((&___BoundsX_10), value);
	}

	inline static int32_t get_offset_of_BoundsZ_11() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964_StaticFields, ___BoundsZ_11)); }
	inline SingleU5BU5D_t1444911251* get_BoundsZ_11() const { return ___BoundsZ_11; }
	inline SingleU5BU5D_t1444911251** get_address_of_BoundsZ_11() { return &___BoundsZ_11; }
	inline void set_BoundsZ_11(SingleU5BU5D_t1444911251* value)
	{
		___BoundsZ_11 = value;
		Il2CppCodeGenWriteBarrier((&___BoundsZ_11), value);
	}

	inline static int32_t get_offset_of_ZoomBounds_12() { return static_cast<int32_t>(offsetof(PinchZoom_t4152403964_StaticFields, ___ZoomBounds_12)); }
	inline SingleU5BU5D_t1444911251* get_ZoomBounds_12() const { return ___ZoomBounds_12; }
	inline SingleU5BU5D_t1444911251** get_address_of_ZoomBounds_12() { return &___ZoomBounds_12; }
	inline void set_ZoomBounds_12(SingleU5BU5D_t1444911251* value)
	{
		___ZoomBounds_12 = value;
		Il2CppCodeGenWriteBarrier((&___ZoomBounds_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHZOOM_T4152403964_H
#ifndef CAMERALOGIC_T2734072073_H
#define CAMERALOGIC_T2734072073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// assets.scripts.cameraLogic.cameraLogic
struct  cameraLogic_t2734072073  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform assets.scripts.cameraLogic.cameraLogic::target
	Transform_t3600365921 * ___target_4;
	// UnityEngine.GameObject assets.scripts.cameraLogic.cameraLogic::camPos
	GameObject_t1113636619 * ___camPos_5;
	// System.Single assets.scripts.cameraLogic.cameraLogic::speed
	float ___speed_6;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(cameraLogic_t2734072073, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_camPos_5() { return static_cast<int32_t>(offsetof(cameraLogic_t2734072073, ___camPos_5)); }
	inline GameObject_t1113636619 * get_camPos_5() const { return ___camPos_5; }
	inline GameObject_t1113636619 ** get_address_of_camPos_5() { return &___camPos_5; }
	inline void set_camPos_5(GameObject_t1113636619 * value)
	{
		___camPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___camPos_5), value);
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(cameraLogic_t2734072073, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERALOGIC_T2734072073_H
#ifndef BILLBOARD_T1300250764_H
#define BILLBOARD_T1300250764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// billboard
struct  billboard_t1300250764  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform billboard::target
	Transform_t3600365921 * ___target_4;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(billboard_t1300250764, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLBOARD_T1300250764_H
#ifndef CAMPOSLOGIC_T1269568684_H
#define CAMPOSLOGIC_T1269568684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// camPosLogic
struct  camPosLogic_t1269568684  : public MonoBehaviour_t3962482529
{
public:
	// System.Single camPosLogic::speed
	float ___speed_4;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(camPosLogic_t1269568684, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMPOSLOGIC_T1269568684_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (FontAssetCreationSettings_t359369028)+ sizeof (RuntimeObject), sizeof(FontAssetCreationSettings_t359369028_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2200[16] = 
{
	FontAssetCreationSettings_t359369028::get_offset_of_sourceFontFileName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_sourceFontFileGUID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_pointSizeSamplingMode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_pointSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_padding_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_packingMode_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_atlasWidth_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_atlasHeight_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_characterSetSelectionMode_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_characterSequence_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_referencedFontAssetGUID_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_referencedTextAssetGUID_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_fontStyle_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_fontStyleModifier_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_renderMode_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_includeFontFeatures_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (KerningPairKey_t536493877)+ sizeof (RuntimeObject), sizeof(KerningPairKey_t536493877 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2201[3] = 
{
	KerningPairKey_t536493877::get_offset_of_ascii_Left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_ascii_Right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_key_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (GlyphValueRecord_t4065874512)+ sizeof (RuntimeObject), sizeof(GlyphValueRecord_t4065874512 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2202[4] = 
{
	GlyphValueRecord_t4065874512::get_offset_of_xPlacement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_yPlacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_xAdvance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_yAdvance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (KerningPair_t2270855589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[5] = 
{
	KerningPair_t2270855589::get_offset_of_m_FirstGlyph_0(),
	KerningPair_t2270855589::get_offset_of_m_FirstGlyphAdjustments_1(),
	KerningPair_t2270855589::get_offset_of_m_SecondGlyph_2(),
	KerningPair_t2270855589::get_offset_of_m_SecondGlyphAdjustments_3(),
	KerningPair_t2270855589::get_offset_of_xOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (KerningTable_t2322366871), -1, sizeof(KerningTable_t2322366871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2204[3] = 
{
	KerningTable_t2322366871::get_offset_of_kerningPairs_0(),
	KerningTable_t2322366871_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
	KerningTable_t2322366871_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (U3CAddKerningPairU3Ec__AnonStorey0_t2688361982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[2] = 
{
	U3CAddKerningPairU3Ec__AnonStorey0_t2688361982::get_offset_of_first_0(),
	U3CAddKerningPairU3Ec__AnonStorey0_t2688361982::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[2] = 
{
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262::get_offset_of_first_0(),
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[2] = 
{
	U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074::get_offset_of_left_0(),
	U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (TMP_FontUtilities_t2599150238), -1, sizeof(TMP_FontUtilities_t2599150238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2208[1] = 
{
	TMP_FontUtilities_t2599150238_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (TMP_VertexDataUpdateFlags_t388000256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2209[8] = 
{
	TMP_VertexDataUpdateFlags_t388000256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (TMP_CharacterInfo_t3185626797)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[35] = 
{
	TMP_CharacterInfo_t3185626797::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_material_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_color_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_style_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (TMP_Vertex_t2404176824)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t2404176824 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2211[5] = 
{
	TMP_Vertex_t2404176824::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (VertexGradient_t345148380)+ sizeof (RuntimeObject), sizeof(VertexGradient_t345148380 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2212[4] = 
{
	VertexGradient_t345148380::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (TMP_PageInfo_t2608430633)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t2608430633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2213[5] = 
{
	TMP_PageInfo_t2608430633::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (TMP_LinkInfo_t1092083476)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[7] = 
{
	TMP_LinkInfo_t1092083476::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (TMP_WordInfo_t3331066303)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[4] = 
{
	TMP_WordInfo_t3331066303::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (TMP_SpriteInfo_t2726321384)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t2726321384 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2216[3] = 
{
	TMP_SpriteInfo_t2726321384::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (Extents_t3837212874)+ sizeof (RuntimeObject), sizeof(Extents_t3837212874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2217[2] = 
{
	Extents_t3837212874::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_t3837212874::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (Mesh_Extents_t3388355125)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t3388355125 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2218[2] = 
{
	Mesh_Extents_t3388355125::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t3388355125::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (WordWrapState_t341939652)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[55] = 
{
	WordWrapState_t341939652::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (TagAttribute_t688278634)+ sizeof (RuntimeObject), sizeof(TagAttribute_t688278634 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2220[3] = 
{
	TagAttribute_t688278634::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (XML_TagAttribute_t1174424309)+ sizeof (RuntimeObject), sizeof(XML_TagAttribute_t1174424309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2221[5] = 
{
	XML_TagAttribute_t1174424309::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (ShaderUtilities_t714255158), -1, sizeof(ShaderUtilities_t714255158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2222[60] = 
{
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Padding_16(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GradientScale_17(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleX_18(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleY_19(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_PerspectiveFilter_20(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureWidth_21(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureHeight_22(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_BevelAmount_23(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowColor_24(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOffset_25(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowPower_26(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOuter_27(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_LightAngle_28(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMap_29(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrix_30(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrixRotation_31(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskCoord_32(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ClipRect_33(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessX_34(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessY_35(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetX_36(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetY_37(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UseClipRect_38(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilID_39(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilOp_40(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilComp_41(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilReadMask_42(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilWriteMask_43(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ShaderFlags_44(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_A_45(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_B_46(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_C_47(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Bevel_48(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Glow_49(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Underlay_50(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Ratios_51(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_SOFT_52(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_HARD_53(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_TEX_54(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Outline_55(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_ZTestMode_56(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_CullMode_57(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_m_clamp_58(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_isInitialized_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2223[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (U24ArrayTypeU3D40_t2865632059)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_t2865632059 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U3CModuleU3E_t692745544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (AnalyticsEvent_t4058973021), -1, sizeof(AnalyticsEvent_t4058973021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_k_SdkVersion_0(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_m_EventData_1(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of__debugMode_2(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_enumRenameTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_4(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2231[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2235[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2239[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_6(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_7(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2241[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2244[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2245[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2246[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2247[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (floorLogic_t3814374729), -1, sizeof(floorLogic_t3814374729_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2255[5] = 
{
	0,
	floorLogic_t3814374729_StaticFields::get_offset_of_allObjects_5(),
	floorLogic_t3814374729_StaticFields::get_offset_of_hiddenObjects_6(),
	floorLogic_t3814374729_StaticFields::get_offset_of_currentBuilding_7(),
	floorLogic_t3814374729::get_offset_of_constantPoints_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (U3CHideFloorComU3Ec__AnonStorey0_t4283171063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[2] = 
{
	U3CHideFloorComU3Ec__AnonStorey0_t4283171063::get_offset_of_floorNumber_0(),
	U3CHideFloorComU3Ec__AnonStorey0_t4283171063::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (pathLogic_t3937024697), -1, sizeof(pathLogic_t3937024697_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2257[13] = 
{
	pathLogic_t3937024697::get_offset_of_lineMat_4(),
	pathLogic_t3937024697::get_offset_of_points_5(),
	pathLogic_t3937024697::get_offset_of_request_6(),
	pathLogic_t3937024697::get_offset_of_path_7(),
	pathLogic_t3937024697::get_offset_of_dropdownList_8(),
	pathLogic_t3937024697::get_offset_of_dropdownListStart_9(),
	pathLogic_t3937024697_StaticFields::get_offset_of_start_10(),
	pathLogic_t3937024697::get_offset_of_dest_11(),
	pathLogic_t3937024697::get_offset_of_url_12(),
	pathLogic_t3937024697::get_offset_of_startingRange_13(),
	pathLogic_t3937024697::get_offset_of_finishingRange_14(),
	pathLogic_t3937024697_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
	pathLogic_t3937024697_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (U3CCalculatePathU3Ec__Iterator0_t2361313786), -1, sizeof(U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2258[10] = 
{
	U3CCalculatePathU3Ec__Iterator0_t2361313786::get_offset_of_U3CformU3E__0_0(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786::get_offset_of_url_1(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786::get_offset_of_U24this_2(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786::get_offset_of_U24current_3(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786::get_offset_of_U24disposing_4(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786::get_offset_of_U24PC_5(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_8(),
	U3CCalculatePathU3Ec__Iterator0_t2361313786_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (U3CCalculatePathU3Ec__AnonStorey1_t2938898595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[2] = 
{
	U3CCalculatePathU3Ec__AnonStorey1_t2938898595::get_offset_of_j_0(),
	U3CCalculatePathU3Ec__AnonStorey1_t2938898595::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (U3CCalculatePathU3Ec__AnonStorey2_t982583459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[3] = 
{
	U3CCalculatePathU3Ec__AnonStorey2_t982583459::get_offset_of_i_0(),
	U3CCalculatePathU3Ec__AnonStorey2_t982583459::get_offset_of_U3CU3Ef__refU240_1(),
	U3CCalculatePathU3Ec__AnonStorey2_t982583459::get_offset_of_U3CU3Ef__refU241_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (pointsLogic_t669785447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[4] = 
{
	pointsLogic_t669785447::get_offset_of_mDropdown_4(),
	pointsLogic_t669785447::get_offset_of_mDropDownStart_5(),
	pointsLogic_t669785447::get_offset_of_points_6(),
	pointsLogic_t669785447::get_offset_of_pointNames_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (billboard_t1300250764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[1] = 
{
	billboard_t1300250764::get_offset_of_target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (cameraLogic_t2734072073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[3] = 
{
	cameraLogic_t2734072073::get_offset_of_target_4(),
	cameraLogic_t2734072073::get_offset_of_camPos_5(),
	cameraLogic_t2734072073::get_offset_of_speed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (camPosLogic_t1269568684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[1] = 
{
	camPosLogic_t1269568684::get_offset_of_speed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (LegendBehaviour_t178662842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[10] = 
{
	LegendBehaviour_t178662842::get_offset_of_panel_4(),
	LegendBehaviour_t178662842::get_offset_of_panel_transform_5(),
	LegendBehaviour_t178662842::get_offset_of_panel_height_6(),
	LegendBehaviour_t178662842::get_offset_of_panel_width_7(),
	LegendBehaviour_t178662842::get_offset_of_panel_pos_x_8(),
	LegendBehaviour_t178662842::get_offset_of_panel_pos_y_9(),
	LegendBehaviour_t178662842::get_offset_of_buttonPrefab_10(),
	LegendBehaviour_t178662842::get_offset_of_font_11(),
	LegendBehaviour_t178662842::get_offset_of_textList_12(),
	LegendBehaviour_t178662842::get_offset_of_sprites_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (PinchZoom_t4152403964), -1, sizeof(PinchZoom_t4152403964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2266[14] = 
{
	PinchZoom_t4152403964::get_offset_of_firstpoint_4(),
	PinchZoom_t4152403964::get_offset_of_secondpoint_5(),
	PinchZoom_t4152403964::get_offset_of_worldStartPoint_6(),
	PinchZoom_t4152403964::get_offset_of_PanSpeed_7(),
	PinchZoom_t4152403964::get_offset_of_ZoomSpeedTouch_8(),
	PinchZoom_t4152403964::get_offset_of_ZoomSpeedMouse_9(),
	PinchZoom_t4152403964_StaticFields::get_offset_of_BoundsX_10(),
	PinchZoom_t4152403964_StaticFields::get_offset_of_BoundsZ_11(),
	PinchZoom_t4152403964_StaticFields::get_offset_of_ZoomBounds_12(),
	PinchZoom_t4152403964::get_offset_of_cam_13(),
	PinchZoom_t4152403964::get_offset_of_lastPanPosition_14(),
	PinchZoom_t4152403964::get_offset_of_panFingerId_15(),
	PinchZoom_t4152403964::get_offset_of_wasZoomingLastFrame_16(),
	PinchZoom_t4152403964::get_offset_of_lastZoomPositions_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (UIBehaviour_t1116137811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[1] = 
{
	UIBehaviour_t1116137811::get_offset_of_mButtons_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (Benchmark01_t1571072624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[10] = 
{
	Benchmark01_t1571072624::get_offset_of_BenchmarkType_4(),
	Benchmark01_t1571072624::get_offset_of_TMProFont_5(),
	Benchmark01_t1571072624::get_offset_of_TextMeshFont_6(),
	Benchmark01_t1571072624::get_offset_of_m_textMeshPro_7(),
	Benchmark01_t1571072624::get_offset_of_m_textContainer_8(),
	Benchmark01_t1571072624::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_t1571072624::get_offset_of_m_material01_12(),
	Benchmark01_t1571072624::get_offset_of_m_material02_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (U3CStartU3Ec__Iterator0_t2216151886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[5] = 
{
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (Benchmark01_UGUI_t3264177817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[10] = 
{
	Benchmark01_UGUI_t3264177817::get_offset_of_BenchmarkType_4(),
	Benchmark01_UGUI_t3264177817::get_offset_of_canvas_5(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TMProFont_6(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TextMeshFont_7(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMeshPro_8(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material01_12(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material02_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (U3CStartU3Ec__Iterator0_t2622988697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[5] = 
{
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (Benchmark02_t1571269232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[3] = 
{
	Benchmark02_t1571269232::get_offset_of_SpawnType_4(),
	Benchmark02_t1571269232::get_offset_of_NumberOfNPC_5(),
	Benchmark02_t1571269232::get_offset_of_floatingText_Script_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (Benchmark03_t1571203696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[3] = 
{
	Benchmark03_t1571203696::get_offset_of_SpawnType_4(),
	Benchmark03_t1571203696::get_offset_of_NumberOfNPC_5(),
	Benchmark03_t1571203696::get_offset_of_TheFont_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (Benchmark04_t1570876016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[5] = 
{
	Benchmark04_t1570876016::get_offset_of_SpawnType_4(),
	Benchmark04_t1570876016::get_offset_of_MinPointSize_5(),
	Benchmark04_t1570876016::get_offset_of_MaxPointSize_6(),
	Benchmark04_t1570876016::get_offset_of_Steps_7(),
	Benchmark04_t1570876016::get_offset_of_m_Transform_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (CameraController_t2264742161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[25] = 
{
	CameraController_t2264742161::get_offset_of_cameraTransform_4(),
	CameraController_t2264742161::get_offset_of_dummyTarget_5(),
	CameraController_t2264742161::get_offset_of_CameraTarget_6(),
	CameraController_t2264742161::get_offset_of_FollowDistance_7(),
	CameraController_t2264742161::get_offset_of_MaxFollowDistance_8(),
	CameraController_t2264742161::get_offset_of_MinFollowDistance_9(),
	CameraController_t2264742161::get_offset_of_ElevationAngle_10(),
	CameraController_t2264742161::get_offset_of_MaxElevationAngle_11(),
	CameraController_t2264742161::get_offset_of_MinElevationAngle_12(),
	CameraController_t2264742161::get_offset_of_OrbitalAngle_13(),
	CameraController_t2264742161::get_offset_of_CameraMode_14(),
	CameraController_t2264742161::get_offset_of_MovementSmoothing_15(),
	CameraController_t2264742161::get_offset_of_RotationSmoothing_16(),
	CameraController_t2264742161::get_offset_of_previousSmoothing_17(),
	CameraController_t2264742161::get_offset_of_MovementSmoothingValue_18(),
	CameraController_t2264742161::get_offset_of_RotationSmoothingValue_19(),
	CameraController_t2264742161::get_offset_of_MoveSensitivity_20(),
	CameraController_t2264742161::get_offset_of_currentVelocity_21(),
	CameraController_t2264742161::get_offset_of_desiredPosition_22(),
	CameraController_t2264742161::get_offset_of_mouseX_23(),
	CameraController_t2264742161::get_offset_of_mouseY_24(),
	CameraController_t2264742161::get_offset_of_moveVector_25(),
	CameraController_t2264742161::get_offset_of_mouseWheel_26(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (CameraModes_t3200559075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[4] = 
{
	CameraModes_t3200559075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (ChatController_t3486202795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[3] = 
{
	ChatController_t3486202795::get_offset_of_TMP_ChatInput_4(),
	ChatController_t3486202795::get_offset_of_TMP_ChatOutput_5(),
	ChatController_t3486202795::get_offset_of_ChatScrollbar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (EnvMapAnimator_t1140999784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[3] = 
{
	EnvMapAnimator_t1140999784::get_offset_of_RotationSpeeds_4(),
	EnvMapAnimator_t1140999784::get_offset_of_m_textMeshPro_5(),
	EnvMapAnimator_t1140999784::get_offset_of_m_material_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U3CStartU3Ec__Iterator0_t1520811813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[5] = 
{
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U3CmatrixU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (ObjectSpin_t341713598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[10] = 
{
	ObjectSpin_t341713598::get_offset_of_SpinSpeed_4(),
	ObjectSpin_t341713598::get_offset_of_RotationRange_5(),
	ObjectSpin_t341713598::get_offset_of_m_transform_6(),
	ObjectSpin_t341713598::get_offset_of_m_time_7(),
	ObjectSpin_t341713598::get_offset_of_m_prevPOS_8(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Rotation_9(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Position_10(),
	ObjectSpin_t341713598::get_offset_of_m_lightColor_11(),
	ObjectSpin_t341713598::get_offset_of_frames_12(),
	ObjectSpin_t341713598::get_offset_of_Motion_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (MotionType_t1905163921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2281[4] = 
{
	MotionType_t1905163921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (ShaderPropAnimator_t3617420994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[4] = 
{
	ShaderPropAnimator_t3617420994::get_offset_of_m_Renderer_4(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_Material_5(),
	ShaderPropAnimator_t3617420994::get_offset_of_GlowCurve_6(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_frame_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (U3CAnimatePropertiesU3Ec__Iterator0_t4041402054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[5] = 
{
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U3CglowPowerU3E__1_0(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24this_1(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24current_2(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24disposing_3(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (SimpleScript_t3279312205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[3] = 
{
	SimpleScript_t3279312205::get_offset_of_m_textMeshPro_4(),
	0,
	SimpleScript_t3279312205::get_offset_of_m_frame_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (SkewTextExample_t3460249701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[4] = 
{
	SkewTextExample_t3460249701::get_offset_of_m_TextComponent_4(),
	SkewTextExample_t3460249701::get_offset_of_VertexCurve_5(),
	SkewTextExample_t3460249701::get_offset_of_CurveScale_6(),
	SkewTextExample_t3460249701::get_offset_of_ShearAmount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (U3CWarpTextU3Ec__Iterator0_t116130919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[13] = 
{
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_ShearValueU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_curveU3E__0_2(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CtextInfoU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMinXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMaxXU3E__1_6(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CverticesU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CmatrixU3E__2_8(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24this_9(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24current_10(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24disposing_11(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (TeleType_t2409835159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[3] = 
{
	TeleType_t2409835159::get_offset_of_label01_4(),
	TeleType_t2409835159::get_offset_of_label02_5(),
	TeleType_t2409835159::get_offset_of_m_textMeshPro_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (U3CStartU3Ec__Iterator0_t3341539328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[7] = 
{
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CtotalVisibleCharactersU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CcounterU3E__0_1(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CvisibleCountU3E__0_2(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (TextConsoleSimulator_t3766250034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[2] = 
{
	TextConsoleSimulator_t3766250034::get_offset_of_m_TextComponent_4(),
	TextConsoleSimulator_t3766250034::get_offset_of_hasTextChanged_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (U3CRevealCharactersU3Ec__Iterator0_t860191687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[8] = 
{
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_textComponent_0(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtextInfoU3E__0_1(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CvisibleCountU3E__0_3(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24this_4(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24current_5(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24disposing_6(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (U3CRevealWordsU3Ec__Iterator1_t1343183262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[9] = 
{
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_textComponent_0(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalWordCountU3E__0_1(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcounterU3E__0_3(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcurrentWordU3E__0_4(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CvisibleCountU3E__0_5(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24current_6(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24disposing_7(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (TextMeshProFloatingText_t845872552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[10] = 
{
	TextMeshProFloatingText_t845872552::get_offset_of_TheFont_4(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_5(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMeshPro_6(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMesh_7(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_transform_8(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_Transform_9(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_cameraTransform_10(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastPOS_11(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastRotation_12(),
	TextMeshProFloatingText_t845872552::get_offset_of_SpawnType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[12] = 
{
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24this_8(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24current_9(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[12] = 
{
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24this_8(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24current_9(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (TextMeshSpawner_t177691618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[4] = 
{
	TextMeshSpawner_t177691618::get_offset_of_SpawnType_4(),
	TextMeshSpawner_t177691618::get_offset_of_NumberOfNPC_5(),
	TextMeshSpawner_t177691618::get_offset_of_TheFont_6(),
	TextMeshSpawner_t177691618::get_offset_of_floatingText_Script_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (TMP_DigitValidator_t573672104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (TMP_ExampleScript_01_t3051742005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[5] = 
{
	TMP_ExampleScript_01_t3051742005::get_offset_of_ObjectType_4(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_isStatic_5(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_m_text_6(),
	0,
	TMP_ExampleScript_01_t3051742005::get_offset_of_count_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (objectType_t4082700821)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2298[3] = 
{
	objectType_t4082700821::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (TMP_FrameRateCounter_t314972976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[10] = 
{
	TMP_FrameRateCounter_t314972976::get_offset_of_UpdateInterval_4(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_LastInterval_5(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_Frames_6(),
	TMP_FrameRateCounter_t314972976::get_offset_of_AnchorPosition_7(),
	TMP_FrameRateCounter_t314972976::get_offset_of_htmlColorTag_8(),
	0,
	TMP_FrameRateCounter_t314972976::get_offset_of_m_TextMeshPro_10(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_frameCounter_transform_11(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_camera_12(),
	TMP_FrameRateCounter_t314972976::get_offset_of_last_AnchorPosition_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
